<?php
/**
 * Created by PhpStorm.
 * User: viktorkrasnov
 * Date: 20.08.17
 * Time: 15:18
 */

namespace Webslon\Bundle\ApiBundle\ORMTraits;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

trait OrmJsonDataTrait
{
    /**
     * @var array|null
     * @ORM\Column(type="json_array", options={"jsonb": true}, nullable=true)
     * @Groups({"groups"})
     */
    private $jsonData;

    /**
     * @return array|null
     */
    public function getJsonData()
    {
        return $this->jsonData;
    }

    /**
     * @param array $jsonData
     * @return $this
     */
    public function setJsonData(array $jsonData): self
    {
        $this->jsonData = $jsonData;

        return $this;
    }
}
