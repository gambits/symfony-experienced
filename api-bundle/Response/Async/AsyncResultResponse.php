<?php


namespace Webslon\Bundle\ApiBundle\Response\Async;


use Nelmio\ApiDocBundle\Annotation\Model;
use phpseclib\Crypt\Base;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Webslon\Bundle\ApiBundle\Enum\AsyncStatusEnum;
use Webslon\Library\Api\Response\BaseResponse;
use Webslon\Library\Serializer\Service\SerializeService;

class AsyncResultResponse extends BaseResponse
{
    /** @var SerializeService */
    private $serializer;

    /**
     * @var boolean
     *
     * @SWG\Property(type="boolean", default="true", description="Признак успешного ответа сервиса")
     */
    public $status;

    /**
     * @var string
     *
     * @SWG\Property(type="object", ref=@Model(type=AsyncStatusEnum::class), description="Результат работы асинхронной функции")
     */
    public $asyncStatus;

    /**
     * @var
     *
     * @SWG\Property(type="", description="Результат успешной работы сервиса, в случае status=true.")
     */
    public $response;

    /**
     * @param string $asyncStatus
     * @return AsyncResultResponse
     */
    public function setAsyncStatus(string $asyncStatus): AsyncResultResponse
    {
        $this->asyncStatus = $asyncStatus;
        return $this;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param mixed $response
     */
    public function setResponse($response): BaseResponse
    {
        $this->response = $response;

        return $this;
    }

    /**
     * @return array
     */
    public function prepareContent(): array
    {
        return [
            'status' => $this->status,
            'asyncStatus' => $this->asyncStatus,
            'response' => $this->response,
            'errors' => $this->getErrors(),
        ];
    }
}
