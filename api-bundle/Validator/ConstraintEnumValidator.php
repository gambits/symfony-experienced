<?php

namespace Webslon\Bundle\ApiBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Webslon\Bundle\ApiBundle\DTO\Enum\BaseEnum;
use Webslon\Bundle\ApiBundle\DBAL\Types\EnumInterface;

/**
 * Class ConstraintEnumValidator
 */
class ConstraintEnumValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /** @var BaseEnum $value */
        if (!$value instanceof EnumInterface) {
            return;
        }
        if ($value->code === null) {
            $value->code = 'NULL';
        }
        $choices = array_keys($value::choices());
        $allowValues = implode(', ', $value::choices());
        if (!in_array($value->code, $choices, true)) {
            $this->context->buildViolation('error.validation.enum')
                ->setParameter('{{class}}', get_class($value))
                ->setParameter('{{value}}', $value->code)
                ->setParameter('{{allow_values}}', $allowValues)
                ->atPath('code')
                ->addViolation();
        }
    }
}
