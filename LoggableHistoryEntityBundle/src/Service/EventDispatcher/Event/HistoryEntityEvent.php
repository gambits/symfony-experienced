<?php

namespace Webslon\Bundle\WebslonEntityHistory\Service\EventDispatcher\Event;

use Webslon\Bundle\WebslonEntityHistory\Entity\HistoryEntity;

/**
 * Class HistoryEntityEvent
 */
class HistoryEntityEvent extends \Symfony\Component\EventDispatcher\Event
{
    /**
     * @var bool
     */
    public $isValid = true;

    /** @var HistoryEntity */
    private $historyEntity;

    /** @var object */
    private $object;

    /**
     * HistoryEntityEvent constructor.
     *
     * @param HistoryEntity $historyEntity
     * @param object        $object - object loggable entity
     */
    public function __construct(HistoryEntity $historyEntity, $object)
    {
        $this->historyEntity = $historyEntity;
        $this->object = $object;

    }

    /**
     * @return HistoryEntity
     */
    public function getHistoryEntity(): HistoryEntity
    {
        return $this->historyEntity;
    }

    /**
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }
}