<?php

namespace Webslon\Bundle\ApiBundle\Controller\AMQP\Response\Model;

use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;
use Webslon\Library\Api\Exception\ApiException;
use Webslon\Bundle\ApiBundle\Controller\AMQP\Response\Model\MessageListResponse;

/**
 * Class MessageQueueResponse
 */
class MessageQueueResponse
{
    /**
     * @var bool
     * @SWG\Property(type="boolean", description="Статус выполнения запроса")
     */
    public $status;

    /**
     * @var array|null
     * @SWG\Property(type="array", description="Массив ошибок, в случае status=false", @SWG\Items(ref=@Model(type=ApiException::class)))
     */
    public $errors;

    /**
     * @var string|null
     * @SWG\Property(type="string", description="Уникальный идентификатор запроса")
     */
    public $requestId;

    /**
     * @var array<MessageListResponse>
     * @Groups({"default"})
     * @SWG\Property(type="object", ref=@Model(type=MessageListResponse::class), description="Сообщения из очереди")
     */
    public $response;

    /**
     * @var int
     * @SWG\Property(type="integer", description="Http код ответа")
     */
    public $httpRequestCode;
}