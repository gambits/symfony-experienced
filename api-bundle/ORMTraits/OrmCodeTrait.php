<?php
/**
 * Created by PhpStorm.
 * User: viktorkrasnov
 * Date: 20.08.17
 * Time: 15:18
 */

namespace Webslon\Bundle\ApiBundle\ORMTraits;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

trait OrmCodeTrait
{
    /**
     * @var string Строковый идентификатор элемента
     *
     * @Groups({
     *     "default",
     *     "api.v1.group.roles",
     *     "api.v1.group.list",
     * })
     *
     * @ORM\Column(type="string", unique=true, nullable=true)
     * @SWG\Property(description="Текстовый идентификатор", type="string")
     */
    private $code;

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param null|string $code
     * @return $this
     */
    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSetCode(): bool
    {
        return !($this->code === null || $this->code === '');
    }
}
