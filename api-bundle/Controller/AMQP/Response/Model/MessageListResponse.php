<?php


namespace Webslon\Bundle\ApiBundle\Controller\AMQP\Response\Model;


use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Webslon\Bundle\ApiBundle\Controller\AMQP\Response\Model\AmqpMessageItemModel;

/**
 * Class MessageListResponse
 */
class MessageListResponse
{
    /**
     * @var integer
     * @SWG\Property(type="integer", description="Кол-во сообщений в очереди")
     */
    public $totalCount;

    /**
     * @var integer
     * @SWG\Property(type="integer", description="Количество выведенных сообщений")
     */
    public $countItems;

    /**
     * @var integer
     * @SWG\Property(type="array", @SWG\Items(ref=@Model(type=AmqpMessageItemModel::class)), description="Массив сообщений из запрошенной очереди")
     */
    public $items;
}