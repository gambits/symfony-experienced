<?php

namespace Webslon\Bundle\ApiBundle\Consumer\Batch;

use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Webslon\Bundle\ApiBundle\AMQP\Exception\AmqpFatalException;
use Webslon\Bundle\ApiBundle\AMQP\Packet;
use Webslon\Bundle\ApiBundle\AMQP\Router\Route;
use Webslon\Bundle\ApiBundle\Consumer\AbstractCrudConsumer;
use Webslon\Bundle\ApiBundle\EventDispatcher\AMQP\AmqpBeforeBatchProcess;
use Webslon\Bundle\ApiBundle\Service\CRUD\ReplaceItemService;
use Webslon\Bundle\ApiBundle\Service\DTO\DTOFactory;
use Webslon\Bundle\ApiBundle\Service\ValidationService;
use Webslon\Library\Api\Exception\ApiException;

class ReplaceConsumer extends AbstractCrudConsumer
{
    /**
     * @var string
     */
    protected $topicName = Route::TOPIC_REPLACE_PREFIX;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ReplaceItemService
     */
    private $replaceItemService;

    /**
     * @var DTOFactory
     */
    private $dtoFactory;

    /**
     * @required
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @required
     * @param DTOFactory $dtoFactory
     * @return $this
     */
    public function setDtoFactory(DTOFactory $dtoFactory): self
    {
        $this->dtoFactory = $dtoFactory;

        return $this;
    }

    /**
     * @required
     * @param ReplaceItemService $replaceItemService
     */
    public function setReplaceItemService(ReplaceItemService $replaceItemService): void
    {
        $this->replaceItemService = $replaceItemService;
    }

    /**
     * Create or replace (full update) entity
     * @param Packet $packet
     *
     * @return void
     * @throws \Exception
     * @throws \ReflectionException
     * @throws \Webslon\Library\Api\Service\HandlerException\Validation\ValidationException
     */
    public function doProcess(Packet $packet)
    {
        $request = new Request();
        $request->setMethod('PATCH');
        $this->replaceItemService->setRequest($request);

        $event = new AmqpBeforeBatchProcess($packet, __CLASS__);
        $this->replaceItemService->getDependencies()->getDispatcher()->dispatch($event);

        $data = $packet->getData();
        $this->entityClass = $data['entity'];
        if ($this->topicName !== $data['action']) {
            $this->reject();
            true;
        }

        try {
            $this->entityManager->getMetadataFactory()->getMetadataFor($this->entityClass);
            $isEntity = true;
        } catch (MappingException $mappingException) {
            $isEntity = false;
        }

        if (!$isEntity) {
            $this->replaceItemService->setDtoClass($this->entityClass);
            $entityClassForDto = $this->dtoFactory->getEntityClassForDto($this->entityClass);
            if (!$entityClassForDto) {
                throw new AmqpFatalException('DTO class '.$this->entityClass.' must use DTO annotation');
            }
            $entityClass = $entityClassForDto;
        } else {
            $entityClass = $this->entityClass;
        }
        $response = [];
        foreach ($data['package'] as $index => $package) {
            try {
                $responseItem = $this->replaceItemService->replace($package['id'], json_encode($package), $entityClass, ValidationService::GROUP_UPDATE_PUT_REPLACE);
                $response[] = [
                    'status' => $responseItem->isStatusOk(),
                    'response' => $this->createSuccessResponse($responseItem),
                    'errors' => [],
                ];
            } catch (\Exception $exception) {
                $response[] = $this->createResponseWitnException($package, $index, $exception);
            } catch (\Throwable $exception) {
                $response[] = $this->createResponseWitnException($package, $index, $exception);
            }
        }

        if (array_filter($this->getReplyData($packet))) {
            $this->reply($response);
            $packet->getAMQPMessage()->set('reply_to', null);
        }
        $this->entityManager->clear();
        gc_collect_cycles();
    }
}