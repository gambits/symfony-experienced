<?php

namespace Webslon\Bundle\ApiBundle\Controller\AMQP;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Webslon\Bundle\ApiBundle\AMQP\RabbitRestClient;
use Webslon\Bundle\ApiBundle\Annotation\Resource;
use Webslon\Bundle\ApiBundle\Service\DependenciesService;
use Webslon\Library\Api\Response\BaseResponse;
use Webslon\Bundle\ApiBundle\Controller\AMQP\Response\Model\MessageQueueResponse;

/**
 * Class MessageController
 * @Route("/amqp", name="app_amqp_get_messages")
 * @Resource(
 *     tags={"AMQP"},
 *     description="AMQP API",
 *     summariesMap={
 *          "getMessageByQueue": "Получить сообщения из очереди",
 *     },
 *     descriptionsMap={
 *          "getMessageByQueue": "Получить сообщения из очереди",
 *     }
 * )
 */
class MessageController extends AbstractController
{
    /**
     * @var RabbitRestClient
     */
    private $rabbitRestClient;
    /**
     * @var DependenciesService
     */
    private $dependenciesService;

    /**
     * @SWG\Get(
     *     tags={"AMQP"},
     *     operationId="getMessageByQueue",
     *     summary="Получить сообщения из очереди",
     *     description="Получить сообщения из очереди",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="host",
     *          in="query",
     *          type="string",
     *          description="Хост"
     *     ),
     *     @SWG\Parameter(
     *          name="queue",
     *          in="query",
     *          type="string",
     *          description="Очередь",
     *          required=false
     *     ),
     *     @SWG\Parameter(
     *          name="count",
     *          in="query",
     *          type="string",
     *          description="Количество сообщений",
     *          required=false
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Успешный ответ сервиса, в результате приходят
     *     данные запрошенного объекта",
     *         @Model(type=MessageQueueResponse::class)
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="Ошибка выполнения операции",
     *         examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": null,
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode":
     *     "ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     )
     * )
     * @Route("/messages", requirements={"host":"\w+", "queue":"\w+"}, defaults={"host":"%2F"})
     * @param Request $request
     *
     * @return Response
     */
    public function getMessage(Request $request, DependenciesService $dependenciesService)
    {
        $this->dependenciesService = $dependenciesService;

        $host = $request->get('host', '%2F');
        if ($host === '/') {
            $host = '%2F';
        }
        $queue = $request->get('queue');
        $count = $request->get('count', 300);
        $this->rabbitRestClient = new RabbitRestClient(getenv('RABBITMQ_HOST'), $host, getenv('RABBITMQ_HTTP_PROTOCOL'), getenv('RABBITMQ_HTTP_PORT'), getenv('RABBITMQ_USERNAME'), getenv('RABBITMQ_PASSWORD'));

        $messages = $this->rabbitRestClient
            ->setBody(['count' => $count,'ackmode' => 'ack_requeue_true', 'encoding' => 'auto', 'truncate' => 50000])
            ->setHeaders(['content-type' => 'application/x-www-form-urlencoded'])
            ->request(Request::METHOD_POST, '/api/queues/'.$host.'/'.$queue.'/get');

        $result = [];
        foreach ($messages as $message) {
            $message['payload'] = json_decode($message['payload'], true);
            $result[] = $message;
        }

        return $this->handleResponse([
            'totalCount' => $messages[0]['message_count'],
            'countItems' => $count,
            'items' => $result,
        ])->send();
    }

    /**
     * @param array               $result
     *
     * @return BaseResponse
     */
    private function handleResponse($result)
    {
        $this->dependenciesService->getResponse()->setResponse($result);

        return $this->dependenciesService->getResponse();
    }
}