<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 28.02.19
 * Time: 9:25
 */

namespace Webslon\Bundle\ApiBundle\DTO;


interface DTOControllerInterface
{
    /** @return object */
    public function createEntity();

    /** @param object $entity */
    public function loadEntity($entity);
}