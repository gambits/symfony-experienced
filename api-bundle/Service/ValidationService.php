<?php

namespace Webslon\Bundle\ApiBundle\Service;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Webslon\Library\Api\Exception\ApiException;
use Webslon\Library\Api\Response\BaseResponse;
use Symfony\Component\Translation\TranslatorInterface;
use \Webslon\Library\Api\Service\HandlerException\Validation\ValidationException;
use \Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationService
{
    const GROUP_DEFAULT = 'Default';
    const GROUP_CREATE = 'createEntity';
    const GROUP_UPDATE = 'updateEntity';
    const GROUP_UPDATE_PUT = 'updateEntityPut';
    const GROUP_UPDATE_PUT_REPLACE = 'replaceEntityPut';

    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var BaseResponse
     */
    private $response;
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * ValidationService constructor.
     *
     * @param ValidatorInterface  $validator
     * @param BaseResponse        $response
     * @param TranslatorInterface $translator
     */
    public function __construct(ValidatorInterface $validator, BaseResponse $response, TranslatorInterface $translator)
    {
        $this->validator = $validator;
        $this->response = $response;
        $this->translator = $translator;
    }

    /**
     * @param mixed             $entity - Default object entity
     * @param null              $constraints
     * @param array|string|null $groups
     * @param boolean           $isThrow
     *
     * @return ConstraintViolationListInterface
     * @throws ValidationException
     */
    public function validate($entity, $constraints = null, $groups = null, $isThrow = true)
    {
        /** @var ConstraintViolationListInterface $errors */
        $errors = $this->validator->validate($entity, $constraints, $groups);

        if (\count($errors) > 0 && $isThrow) {
            throw new ValidationException($errors);
        }

        return $errors;
    }

    /**
     * @param mixed  $value  - Default object entity
     * @param null   $constraints
     * @param null   $groups
     * @param string $domain - domain param for translator
     *
     * @return BaseResponse
     */
    public function validateAndGetResponse($value, $constraints = null, $groups = null, $domain = 'messages')
    {
        /** @var ConstraintViolationListInterface $errors */
        $errors = $this->validator->validate($value, $constraints, $groups, false);

        if (count($errors)) {
            $this->response
                ->setErrors($this->mapError($errors))
                ->setHttpResponseCode(Response::HTTP_BAD_REQUEST)
                ->statusError();
        }

        return $this->response;
    }

    /**
     * @param array ...$errors
     *
     * @return array
     */
    public function mapError(...$errors)
    {
        $map = $errors; // default
        foreach ($errors as $violations) {
            if (is_array($violations)) {
                foreach ($violations as $field => $message) {
                    $map[$field] = $this->translator->trans($message, ['field' => $field]);
                }
            } elseif ($violations instanceof ConstraintViolationList) {
                foreach ($violations as $violation) {
                    $map[$violation->getPropertyPath()] = $this->translator->trans($violation->getMessage(), ['field' => $violation->getPropertyPath()]);
                }
            }
        }

        return $map;
    }

    /**
     * @param array $message
     * @param array $value
     *
     * @return ApiException
     */
    protected function createException($message, $value = [])
    {
        return new ApiException(implode(', ', $message), 'Error_' . Response::HTTP_BAD_REQUEST, $value, Response::HTTP_BAD_REQUEST);
    }
}