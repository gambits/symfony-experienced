<?php

namespace Webslon\Bundle\ApiBundle;

use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Webslon\Bundle\ApiBundle\DependencyInjection\Compiler\AmqpConsumerPass;
use Webslon\Bundle\ApiBundle\DependencyInjection\Compiler\ConfigurationPass;
use Webslon\Bundle\ApiBundle\DependencyInjection\Compiler\EnqueueBatchSubscriberPass;
use Webslon\Bundle\ApiBundle\DependencyInjection\Compiler\EnqueueProducerPass;
use Webslon\Bundle\ApiBundle\DependencyInjection\Compiler\EnqueueSubscriberPass;
use Webslon\Bundle\ApiBundle\DependencyInjection\Compiler\FixSentryExceptionListenerPass;

class WebslonApiBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ConfigurationPass());
        $container->addCompilerPass(new EnqueueSubscriberPass());
        $container->addCompilerPass(new EnqueueBatchSubscriberPass());
        $container->addCompilerPass(new EnqueueProducerPass());
        $container->addCompilerPass(new AmqpConsumerPass());
        $container->addCompilerPass(new FixSentryExceptionListenerPass());
    }

    public function getParent()
    {
        return 'NelmioApiDocBundle';
    }
}
