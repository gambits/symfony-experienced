<?php

namespace Webslon\Bundle\WebslonEntityHistory;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class WebslonEntityHistoryBundle
 */
class WebslonEntityHistoryBundle extends Bundle
{
}