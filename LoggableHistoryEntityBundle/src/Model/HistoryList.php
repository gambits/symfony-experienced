<?php

namespace Webslon\Bundle\WebslonEntityHistory\Model;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Webslon\Bundle\WebslonEntityHistory\Entity\HistoryEntity;

/**
 * Class HistoryList
 */
class HistoryList
{
    /**
     * @var array<HistoryEntity>
     * @SWG\Property(description="Массив записей истории", type="array", @SWG\Items(ref=@Model(type=HistoryEntity::class)))
     */
    public $items;

    /**
     * @var integer
     * @SWG\Property(description="Количество найденных записей, с учетом фильтра", type="integer")
     */
    public $totalCount = 0;
}