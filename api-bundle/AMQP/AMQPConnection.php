<?php

namespace Webslon\Bundle\ApiBundle\AMQP;

use PhpAmqpLib\Connection\AMQPLazyConnection;

/**
 * Class AMQPConnection
 */
class AMQPConnection extends AMQPLazyConnection
{
}