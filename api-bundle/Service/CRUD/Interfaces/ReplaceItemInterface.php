<?php


namespace Webslon\Bundle\ApiBundle\Service\CRUD\Interfaces;

use Webslon\Library\Api\Exception\ApiException;
use Webslon\Library\Api\Response\BaseResponse;
use Webslon\Library\Api\Response\BaseResponse as ApiResponse;
use Webslon\Library\Api\Service\HandlerException\Validation\ValidationException;

/**
 * Interface ReplaceItemInterface
 */
interface ReplaceItemInterface
{
    /**
     * @param string            $id
     * @param string            $data
     * @param string            $objectName
     * @param array|string|null $validationGroup
     * @param array             $outputSerializationContext
     * @param array             $inputSerializationContext
     *
     * @return BaseResponse
     * @throws ApiException
     * @throws ValidationException
     */
    public function replace(string $id, string $data, string $objectName, $validationGroup = null, array $outputSerializationContext = [], array $inputSerializationContext = []): ApiResponse;
}