<?php

namespace Webslon\Bundle\ApiBundle\Swagger;

use Nelmio\ApiDocBundle\Annotation\Model as ModelAnnotation;
use Nelmio\ApiDocBundle\Model\Model;
use Nelmio\ApiDocBundle\Model\ModelRegistry;
use Swagger\Analysis;
use Swagger\Annotations\Response;
use Swagger\Annotations\Schema;

/**
 * Если есть смерженные модели, то возможно потребуется "на ходу" менять контекст
 * Т.к. изначально идет регистрация моделей в ModelRegister
 * И только лишь потом создание схем для Swagger.
 *
 * Т.к. все классы final, то нам придется повторить эту логику.
 *
 * Class CheckingUnmergedModelRegister
 */
class CheckingUnmergedModelRegister
{
    private $modelRegistry;

    public function __construct(ModelRegistry $modelRegistry)
    {
        $this->modelRegistry = $modelRegistry;
    }

    public function __invoke(Analysis $analysis)
    {
        foreach ($analysis->annotations as $annotation) {
            if ($annotation instanceof Response) {
                $annotationClass = Schema::class;
            } else {
                continue;
            }

            $model = null;
            foreach ($annotation->_unmerged as $unmerged) {
                if ($unmerged instanceof ModelAnnotation) {
                    $model = $unmerged;

                    break;
                }
            }

            if (null === $model || !$model instanceof ModelAnnotation) {
                continue;
            }

            if (!is_string($model->type)) {
                continue;
            }

            $mergedParams = [];
            foreach ($annotation->parameters as &$parameter) {
                if (!isset($mergedParams[get_class($parameter)])) {
                    $parameter->_unmerged[] = $model;
                    $mergedParams[get_class($parameter)] = 1;
                }

                if ($parameter->schema && $parameter->schema->ref == '#/definitions/SomeObject') {
                    continue;
                }
            }

            $annotation->merge([new $annotationClass([
                'ref' => $this->modelRegistry->register(new Model($this->createType($model->type), $model->groups)),
            ])]);

            foreach ($annotation->_unmerged as $key => $unmerged) {
                if ($unmerged === $model) {
                    unset($annotation->_unmerged[$key]);

                    break;
                }
            }

            if ($analysis->annotations->contains($model)) {
                $analysis->annotations->detach($model);
            }
        }
    }
}
