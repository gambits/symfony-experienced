<?php

namespace Webslon\Bundle\ApiBundle\AMQP\Exception;

/**
 * Class AmqpFatalException
 */
class AmqpFatalException extends \RuntimeException
{

}