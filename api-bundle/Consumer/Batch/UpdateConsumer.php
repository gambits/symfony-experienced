<?php

namespace Webslon\Bundle\ApiBundle\Consumer\Batch;

use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Webslon\Bundle\ApiBundle\AMQP\Exception\AmqpFatalException;
use Webslon\Bundle\ApiBundle\AMQP\Exception\AmqpNotFoundEntityException;
use Webslon\Bundle\ApiBundle\AMQP\Packet;
use Webslon\Bundle\ApiBundle\AMQP\Router\Route;
use Webslon\Bundle\ApiBundle\Consumer\AbstractCrudConsumer;
use Webslon\Bundle\ApiBundle\EventDispatcher\AMQP\AmqpBeforeBatchProcess;
use Webslon\Bundle\ApiBundle\Exception\AmqpConsumerEntityIsEmptyException;
use Webslon\Bundle\ApiBundle\Service\CRUD\UpdateItemService;
use Webslon\Bundle\ApiBundle\Service\DTO\DTOFactory;
use Webslon\Bundle\ApiBundle\Service\ValidationService;
use Webslon\Library\Api\Exception\ApiException;

class UpdateConsumer extends AbstractCrudConsumer
{
    /**
     * @var string
     */
    protected $topicName = Route::TOPIC_UPDATE_PREFIX;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UpdateItemService
     */
    private $updateItemService;

    /**
     * @var DTOFactory
     */
    private $dtoFactory;

    /**
     * @required
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @required
     * @param DTOFactory $dtoFactory
     * @return $this
     */
    public function setDtoFactory(DTOFactory $dtoFactory): self
    {
        $this->dtoFactory = $dtoFactory;

        return $this;
    }

    /**
     * @required
     * @param UpdateItemService $updateItemService
     */
    public function setUpdateItemService(UpdateItemService $updateItemService)
    {
        $this->updateItemService = $updateItemService;
    }

    /**
     * @param Packet $packet
     *
     * @return void
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function doProcess(Packet $packet)
    {
        $request = new Request();
        $request->setMethod('PATCH');
        $this->updateItemService->setRequest($request);

        $event = new AmqpBeforeBatchProcess($packet, __CLASS__);
        $this->updateItemService->getDependencies()->getDispatcher()->dispatch($event);

        $updateData = $packet->getData();
        $this->entityClass = $updateData['entity'];
        if ($this->topicName !== $updateData['action']) {
            $this->reject();

            return;
        }

        try {
            $this->entityManager->getMetadataFactory()->getMetadataFor($this->entityClass);
            $isEntity = true;
        } catch (MappingException $mappingException) {
            $isEntity = false;
        }

        if (!$isEntity) {
            $this->updateItemService->setDtoClass($this->entityClass);

            $entityClassForDto = $this->dtoFactory->getEntityClassForDto($this->entityClass);
            if (!$entityClassForDto) {
                throw new AmqpFatalException('DTO class '.$this->entityClass.' must use DTO annotation');
            }
            $entityClass = $entityClassForDto;
        } else {
            $entityClass = $this->entityClass;
        }
        $response = [];
        foreach ($updateData['package'] as $index => $package) {
            $entityId = $package['id'];
            $entity = $this->entityManager->find($entityClass, $entityId);
            try {
                if (!$entity) {
                    throw (new AmqpNotFoundEntityException('Entity ('.$entityClass.') by id='.$entityId.' not found', 'Error_404', null, Response::HTTP_NOT_FOUND))
                        ->setEntityId($entityId);
                }
                $responseItem = $this->updateItemService->update($entityId, json_encode($package), $entityClass, ValidationService::GROUP_UPDATE);
                $response[] = [
                    'status' => $responseItem->isStatusOk(),
                    'response' => $this->createSuccessResponse($responseItem),
                    'errors' => [],
                ];
            } catch (\Exception $exception) {
                $response[] = $this->createResponseWitnException($package, $index, $exception);
            } catch (\Throwable $exception) {
                $response[] = $this->createResponseWitnException($package, $index, $exception);
            }
        }

        if (array_filter($this->getReplyData($packet))) {
            $this->reply($response);
            $packet->getAMQPMessage()->set('reply_to', null);
        }

        $this->entityManager->clear();
        gc_collect_cycles();
    }
}