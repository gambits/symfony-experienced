<?php


namespace Webslon\Bundle\ApiBundle\Service\CRUD\Interfaces;

use Webslon\Library\Api\Exception\ApiException;
use Webslon\Library\Api\Response\BaseResponse as ApiResponse;

/**
 * Interface DeleteItemInterface
 */
interface DeleteItemInterface
{
    /**
     * @param string $id
     * @param string $object
     * @param array  $serializationContext
     *
     * @return ApiResponse
     * @throws ApiException
     */
    public function deleteItem(string $id, string $object, $serializationContext): ApiResponse;
}