<?php


namespace Webslon\Bundle\ApiBundle\Consumer\Batch;

use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Webslon\Bundle\ApiBundle\AMQP\Packet;
use Webslon\Bundle\ApiBundle\AMQP\Router\Route;
use Webslon\Bundle\ApiBundle\Consumer\AbstractCrudConsumer;
use Webslon\Bundle\ApiBundle\EventDispatcher\AMQP\AmqpBeforeBatchProcess;
use Webslon\Bundle\ApiBundle\Exception\AmqpConsumerEntityIsEmptyException;
use Webslon\Bundle\ApiBundle\Service\CRUD\AddItemService;
use Webslon\Bundle\ApiBundle\Service\ValidationService;
use Webslon\Library\Api\Response\BaseResponse;
use Webslon\Library\Serializer\Service\SerializeService;

/**
 * Class CreateConsumer
 */
class CreateConsumer extends AbstractCrudConsumer
{
    /**
     * @var string
     */
    protected $topicName = Route::TOPIC_CREATE_PREFIX;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var AddItemService
     */
    private $addItemManager;

    /**
     * @var SerializerInterface|Serializer
     */
    private $serializer;

    /**
     * @required
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager (EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @required
     * @param AddItemService $addItemManager
     */
    public function setAddItemManager (AddItemService $addItemManager)
    {
        $this->addItemManager = $addItemManager;
    }

    /**
     * @required
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param Packet $packet
     * @return boolean
     * @throws \Exception
     */
    public function doProcess(Packet $packet)
    {
        $request = new Request();
        $request->setMethod('POST');
        $this->addItemManager->setRequest($request);

        $event = new AmqpBeforeBatchProcess($packet, __CLASS__);
        $this->addItemManager->getDependencies()->getDispatcher()->dispatch($event);

        $creationData = $packet->getData();
        $this->entityClass = $creationData['entity'];
        if ($this->topicName !== $creationData['action']) {
            $this->reject();

            return;
        }
        if ($this->validate($creationData)) {
            try {
                $this->entityManager->getMetadataFactory()->getMetadataFor($this->entityClass);
                $isEntity = true;
            } catch (MappingException $mappingException) {
                $isEntity = false;
            }

            if (!$isEntity) {
                $this->addItemManager->setDtoClass($this->entityClass);
            }
            $response = [];
            foreach ($creationData['package'] as $index => $package) {
                try {
                    $responseItem = $this->addItemManager->add(json_encode($package), $this->entityClass, ValidationService::GROUP_CREATE);
                    $response[] = [
                        'status' => $responseItem->isStatusOk(),
                        'response' => $this->createSuccessResponse($responseItem),
                        'errors' => null
                    ];
                } catch (\Exception $exception) {
                    $response[] = $this->createResponseWitnException($package, $index, $exception);
                } catch (\Throwable $exception) {
                    $response[] = $this->createResponseWitnException($package, $index, $exception);
                }
            }

            if (array_filter($this->getReplyData($packet))) {
                $this->reply($response);
                $packet->getAMQPMessage()->set('reply_to', null);
            }
            $this->entityManager->clear();
            gc_collect_cycles();

            return true;
        }
    }

    /**
     * @param array $creationData
     *
     * @return bool
     * @throws AmqpConsumerEntityIsEmptyException
     */
    private function validate(array $creationData):bool
    {
        if (!$this->entityClass = $creationData['entity']) {
            throw new AmqpConsumerEntityIsEmptyException('amqp.exception.entity.is_null');
        }
        if ($this->topicName !== $creationData['action']) {
            $this->reject();

            return false;
        }
        if (!$creationData['package']) {
            $this->ack();

            return false;
        }

        return true;
    }
}
