<?php

namespace Webslon\Bundle\MessageExchange\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;
use Webslon\Bundle\ApiBundle\ORMTraits\OrmUpdatedAtTrait;
use Webslon\Bundle\ApiBundle\ORMTraits\OrmCreatedAtTrait;
use Webslon\Bundle\ApiBundle\ORMTraits\OrmIdTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="Webslon\Bundle\MessageExchange\Repository\MessageExchangeRepository")
 * @ORM\Table(indexes={
 *     @ORM\Index(name="xhash", columns={"hash"}),
 *     @ORM\Index(name="xstatus", columns={"status"}),
 *     @ORM\Index(name="xtype", columns={"type"}),
 * })
 */
class MessageExchange
{
    use OrmIdTrait;
    use OrmCreatedAtTrait;
    use OrmUpdatedAtTrait;
    public const TYPE_DEFAULT = 1;
    public const STATUS_PENDING = 'pending';
    public const STATUS_FINISHED = 'finished';
    public const STATUS_ERROR = 'error';

    /**
     * @var string
     * @Assert\NotNull()
     * @ORM\Column(type="string", length=255)
     */
    private $route;

    /**
     * @var string
     * @Assert\NotNull()
     * @Assert\Choice(callback="getChoicesMethod")
     * @ORM\Column(type="string", length=255)
     */
    private $method;

    /**
     * @var array
     * @Assert\Choice(callback="getAvailableOptionChoices")
     * @ORM\Column(type="json_array", nullable=true, options={"comment":"DC2Type:json_array"})
     */
    private $options;

    /**
     * @var integer
     * @ORM\Column(type="integer", length=255, options={"default"=1})
     */
    private $type = self::TYPE_DEFAULT;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $counter = 0;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $status = self::STATUS_PENDING;

    /**
     * @var array|null
     * @ORM\Column(type="json_array", name="error_data", nullable=true)
     */
    private $errorData;

    /**
     * @var string
     * @ORM\Column(type="string", name="hash", nullable=false)
     */
    private $hash;

    /**
     * MessageExchange constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return null|string
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @param string $route
     *
     * @return MessageExchange
     */
    public function setRoute(string $route): self
    {
        $this->route = $route;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     *
     * @return MessageExchange
     */
    public function setOptions($options): self
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param integer|null $type
     *
     * @return MessageExchange
     */
    public function setType($type): self
    {
        if ($type !== null) {
            $this->type = $type;
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCounter(): ?int
    {
        return $this->counter;
    }

    /**
     * @param int|null $counter
     *
     * @return MessageExchange
     */
    public function setCounter(?int $counter): self
    {
        if (!$counter) {
            $counter = 0;
        }
        $this->counter = $counter;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return MessageExchange
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getErrorData(): ?array
    {
        return $this->errorData;
    }

    /**
     * @param array|null $errorData
     *
     * @return MessageExchange
     */
    public function setErrorData(?array $errorData): self
    {
        $this->errorData = $errorData;

        return $this;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return array
     */
    public static function getAvailableOptionChoices()
    {
        return ['body', 'filter', 'query', 'request', 'headers', 'form_params'];
    }

    /**
     * @return array
     */
    public static function getChoicesMethod()
    {
        return [
            Request::METHOD_GET, Request::METHOD_POST, Request::METHOD_PATCH, Request::METHOD_DELETE,
        ];
    }
}
