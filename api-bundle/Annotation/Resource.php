<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 23.11.17
 * Time: 10:05.
 */

namespace Webslon\Bundle\ApiBundle\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
class Resource
{
    /**
     * @var array массив тегов
     */
    public $tags = [];

    /**
     * @var string описание
     */
    public $description = '';

    /**
     * @var array
     */
    public $summariesMap = [];

    /**
     * @var string
     */
    public $modelDefinitionName;

    /**
     * @var array описание для различных методов
     */
    public $descriptionsMap = [];

    /**
     * @var array устаревшие функции, которые нужно пометить как deprecated
     */
    public $deprecatedMap = [];

    /**
     * @var array возможные ответы
     */
    public $responsesMap = [];

    /**
     * @var string class name
     */
    public $listResponseModel;

    /**
     * @var string class name
     */
    public $responseModel;

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getDescriptionsMap()
    {
        return $this->descriptionsMap;
    }

    public function getDescriptionMap($id)
    {
        return !empty($this->descriptionsMap[$id]) ? $this->descriptionsMap[$id] : '';
    }

    public function getSummary($id)
    {
        return !empty($this->summariesMap[$id]) ? $this->summariesMap[$id] : '';
    }

    public function getResponses($id)
    {
        return !empty($this->responsesMap[$id]) ? $this->responsesMap[$id] : '';
    }

    public function isDeprecated($id)
    {
        return is_array($this->deprecatedMap) ? array_search($id, $this->deprecatedMap) !== false : false;
    }
}
