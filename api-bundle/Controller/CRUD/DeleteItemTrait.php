<?php

namespace Webslon\Bundle\ApiBundle\Controller\CRUD;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Webslon\Bundle\ApiBundle\Annotation\Operation;
use Webslon\Bundle\ApiBundle\Service\CRUD\DeleteItemService;
use Webslon\Bundle\ApiBundle\Service\SerializationContextFetcher;
use Webslon\Library\Api\Exception\ApiException;

/**
 * Trait DeleteItemTrait
 */
trait DeleteItemTrait
{
    /**
     * @Route("/{id}/", requirements={"id": "[a-z0-9\-\w+]{1,}"}, methods={"DELETE"})
     * @Operation("delete")
     * @SWG\Delete(
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="Идентификатор удаляемой сущности",
     *          type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Сущность успешно удалена",
     *         examples={
     *              "application/json":{
     *                  "status": true,
     *                  "response": null,
     *                  "errors": null
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="Ошибка выполнения операции",
     *         examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": null,
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode":
     *     "ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     )
     * )
     * @param string                      $id
     * @param DeleteItemService           $service
     * @param SerializationContextFetcher $serializationContextFetcher
     *
     * @return Response
     * @throws ApiException
     */
    public function deleteItemAction(string $id, DeleteItemService $service, SerializationContextFetcher $serializationContextFetcher): Response
    {
        $serializationContext = [];

        $entityClass = $this instanceof DynamicEntityClassControllerInterface ? $this->getEntityClass() : self::ENTITY_CLASS;

        if ($groups = $serializationContextFetcher->getSerializationGroups('delete', $entityClass)) {
            $serializationContext['groups'] = $groups;
        }

        $result = $service->deleteItem($id, $entityClass, $serializationContext);

        return $result->send();
    }
}
