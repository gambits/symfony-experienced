<?php

namespace Webslon\Bundle\ApiBundle\Service\CRUD\Filters\FilterModel;

use Webslon\Bundle\ApiBundle\Service\CRUD\Filters\FilterModelInterface;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Class CommonModel
 */
class CommonModel implements FilterModelInterface
{
    /**
     * List fields add to result
     * 
     * @var string
     */
    public $fields;
    
    /**
     * List filters
     *
     * @var string
     */
    public $filter = [];
    /**
     * @var string
     */
    public $group = [];
    /**
     * @var string
     */
    public $order = [];
    /**
     * @var string
     */
    public $limit = 20;
    /**
     * смещение для limit
     * @var string
     */
    public $offset = 0;
    /**
     * Format file to download
     *
     * @var string
     */
    public $download = '';
    
    /**
     * Allowed filers
     *
     * @return array
     */
    public function getAllowedFilters()
    {
        return [
            ConstantFilter::FIELDS,
            ConstantFilter::FILTER,
            ConstantFilter::ORDER,
            ConstantFilter::GROUP,
            ConstantFilter::LIMIT,
            ConstantFilter::OFFSET,
            ConstantFilter::DOWNLOAD,
        ];
    }
}
