<?php

namespace Webslon\Bundle\WebslonEntityHistory\Annotation;

/**
 * Class History
 * @Annotation
 */
final class History
{
    /**
     * @var string
     */
    public $alias;

    /**
     * Field fixed to property Webslon\Bundle\WebslonEntityHistory\Entity::$objectId
     * @var string
     */
    public $fieldIdentifier;
    /**
     * Relation entities for show log
     * @var array
     */
    public $relationField;
    /**
     * @var string
     */
    public $entity;
}