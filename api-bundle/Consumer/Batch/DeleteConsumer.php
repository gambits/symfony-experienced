<?php


namespace Webslon\Bundle\ApiBundle\Consumer\Batch;

use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Webslon\Bundle\ApiBundle\AMQP\Exception\AmqpFatalException;
use Webslon\Bundle\ApiBundle\AMQP\Exception\AmqpNotFoundEntityException;
use Webslon\Bundle\ApiBundle\AMQP\Packet;
use Webslon\Bundle\ApiBundle\AMQP\Router\Route;
use Webslon\Bundle\ApiBundle\Consumer\AbstractCrudConsumer;
use Webslon\Bundle\ApiBundle\EventDispatcher\AMQP\AmqpBeforeBatchProcess;
use Webslon\Bundle\ApiBundle\Exception\AmqpConsumerEntityIsEmptyException;
use Webslon\Bundle\ApiBundle\Exception\AmqpConsumerMessageInvalidStructureException;
use Webslon\Bundle\ApiBundle\Service\CRUD\DeleteItemService;
use Webslon\Bundle\ApiBundle\Service\DTO\DTOFactory;

/**
 * Class DeleteConsumer
 */
class DeleteConsumer extends AbstractCrudConsumer
{
    /**
     * @var string
     */
    protected $topicName = Route::TOPIC_DELETE_PREFIX;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var DeleteItemService
     */
    protected $deleteItemService;

    /**
     * @var DTOFactory
     */
    private $dtoFactory;

    /**
     * @required
     * @param EntityManagerInterface $entityManager
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @required
     * @param DTOFactory $dtoFactory
     * @return $this
     */
    public function setDtoFactory(DTOFactory $dtoFactory): self
    {
        $this->dtoFactory = $dtoFactory;

        return $this;
    }

    /**
     * @required
     * @param DeleteItemService $deleteItemService
     */
    public function setDeleteItemService(DeleteItemService $deleteItemService)
    {
        $this->deleteItemService = $deleteItemService;
    }

    /**
     * @param Packet $packet
     *
     * @throws AmqpConsumerEntityIsEmptyException
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function doProcess(Packet $packet)
    {
        $request = new Request();
        $request->setMethod('DELETE');
        $this->deleteItemService->setRequest($request);

        $event = new AmqpBeforeBatchProcess($packet, __CLASS__);
        $this->deleteItemService->getDependencies()->getDispatcher()->dispatch($event);
        $data = $packet->getData();
        $this->entityClass = $data['entity'];
        if ($this->topicName !== $data['action']) {
            $this->reject();

            return;
        }
        try {
            $this->entityManager->getMetadataFactory()->getMetadataFor($this->entityClass);
            $isEntity = true;
        } catch (MappingException $mappingException) {
            $isEntity = false;
        }

        if (!$isEntity) {
            $this->deleteItemService->setDtoClass($this->entityClass);

            $entityClassForDto = $this->dtoFactory->getEntityClassForDto($this->entityClass);
            if (!$entityClassForDto) {
                throw new AmqpFatalException('DTO class '.$this->entityClass.' must use DTO annotation');
            }

            $entityClass = $entityClassForDto;
        } else {
            $entityClass = $this->entityClass;
        }
        $response = [];
        foreach ($data['package'] as $index => $package) {
            try{
                if (!$entityId = $package['id']) {
                    throw new AmqpConsumerMessageInvalidStructureException();
                }
                $entity = $this->entityManager->find($entityClass, $entityId);
                if (!$entity) {
                    throw (new AmqpNotFoundEntityException('Entity ('.$entityClass.') by id='.$entityId.' not found', 'Error_404', null, Response::HTTP_NOT_FOUND))
                        ->setEntityId($entityId);
                }

                $responseItem = $this->deleteItemService->deleteItem($entityId, $entityClass, []);
                $this->entityManager->refresh($entity);
                $response[] = [
                    'status' => $responseItem->isStatusOk(),
                    'response' => $this->createSuccessResponse($responseItem->setResponse($entity)),
                    'errors' => [],
                ];
            } catch (\Exception $exception) {
                $response[] = $this->createResponseWitnException($package, $index, $exception);
            } catch (\Throwable $exception) {
                $response[] = $this->createResponseWitnException($package, $index, $exception);
            }
        }
        if (array_filter($this->getReplyData($packet))) {
            $this->reply($response);
            $packet->getAMQPMessage()->set('reply_to', null);
        }
        $this->entityManager->clear();
        gc_collect_cycles();
    }
}
