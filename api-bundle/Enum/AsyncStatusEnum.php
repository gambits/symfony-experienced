<?php


namespace Webslon\Bundle\ApiBundle\Enum;

use Webslon\Bundle\ApiBundle\Annotation\EnumAnnotation;
use Webslon\Bundle\ApiBundle\DTO\Enum\BaseEnum;

/**
 * Class AsyncResponseStatusEnum
 * @EnumAnnotation()
 * @package Webslon\Bundle\ApiBundle\Enum
 */
class AsyncStatusEnum extends BaseEnum
{
    protected $enumClass = self::class;

    public const CREATED = 'CREATED';

    public const WAIT = 'WAIT';

    public const DONE = 'DONE';

    public const ERROR = 'ERROR';

    /**
     * @return array Перевод значений на язык системы
     */
    protected static $choices = [
        self::CREATED => 'enum.created',
        self::WAIT => 'enum.wait',
        self::DONE => 'enum.done',
        self::ERROR => 'enum.error',
    ];
}
