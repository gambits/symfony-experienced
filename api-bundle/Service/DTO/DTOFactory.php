<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 28.02.19
 * Time: 8:01
 */

namespace Webslon\Bundle\ApiBundle\Service\DTO;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webslon\Bundle\ApiBundle\Annotation\DTO;
use Webslon\Bundle\ApiBundle\DTO\DTOControllerInterface;

/**
 * Class DTOFactory
 */
class DTOFactory
{
    /** @var ContainerInterface */
    private $container;

    /** @var Reader */
    private $annotationReader;

    /**
     * DTOFactory constructor.
     * @param ContainerInterface $container
     * @param Reader $annotationReader
     */
    public function __construct(ContainerInterface $container, Reader $annotationReader)
    {
        $this->container = $container;
        $this->annotationReader = $annotationReader;
    }

    /**
     * @param string $dtoClass
     * @return DTOControllerInterface
     */
    public function getDTO(string $dtoClass)
    {
        if ($this->container->has($dtoClass)) {
            $object = clone $this->container->get($dtoClass);
        } else {
            $object = new $dtoClass();
        }

        if (!$object instanceof DTOControllerInterface) {
            throw new \RuntimeException(sprintf('Class %s must implement %s', $dtoClass, DTOControllerInterface::class));
        }

        return $object;
    }

    /**
     * @param $dto
     * @return string|null
     * @throws \ReflectionException
     */
    public function getEntityClassForDto($dto)
    {
        $dtoMetadata = $this->getDtoAnnotation($dto);
        if (!$dtoMetadata) {
            return null;
        }

        return $dtoMetadata->entityClass;
    }

    /**
     * @param object|string $dto
     * @return DTO|null
     * @throws \ReflectionException
     */
    private function getDtoAnnotation($dto)
    {
        $class = is_object($dto) ? get_class($dto) : $dto;

        return $this->annotationReader->getClassAnnotation(new \ReflectionClass($class), DTO::class);
    }
}