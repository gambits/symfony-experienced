<?php

namespace Webslon\Bundle\ApiBundle\Swagger;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Model\ModelRegistry;
use Swagger\Analysis;
use Swagger\Annotations\Operation;
use Swagger\Annotations\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouteCollection;
use Webslon\Bundle\ApiBundle\Controller\CRUD\DynamicEntityClassControllerInterface;

class ModelRegister
{
    /** @var ModelRegistry */
    private $modelRegistry;

    /** @var RouteCollection */
    private $routeCollection;

    /** @var ContainerInterface */
    private $container;

    public function __construct(
        RouteCollection $routeCollection,
        ContainerInterface $container
    ) {
        $this->routeCollection = $routeCollection;
        $this->container = $container;
    }

    public function __invoke(Analysis $analysis)
    {
        $merged = [];

        $routes = $this->routeCollection->all();
        foreach ($analysis->annotations as $annotation) {
            if ($annotation instanceof Operation) {
                foreach ($routes as $route) {
                    if ($route->getPath() == $annotation->path) {
                        list($controller, $action) = explode('::', $route->getDefault('_controller'));

                        if ($this->container->has($controller)) {
                            $controllerObj = $this->container->get($controller);
                        } else {
                            $controllerObj = new $controller();
                        }

                        $entityClass = null;
                        if (defined(get_class($controllerObj) . "::ENTITY_CLASS")) {
                            $entityClass = $controllerObj::ENTITY_CLASS;
                        }
                        if ($controllerObj instanceof DynamicEntityClassControllerInterface) {
                            $entityClass = $controllerObj::ENTITY_CLASS;
                        }
                        if (!$entityClass) {
                            continue;
                        }
                        
                        $model = new Model([]);
                        $model->type = $entityClass;

                        $definition = '#/definitions/'.substr(strrchr($controllerObj::ENTITY_CLASS, '\\'), 1);

                        /** @var Response $response */
                        foreach ($annotation->responses as &$response) {
                            $response->_unmerged[] = $model;
                            $merged[$controllerObj::ENTITY_CLASS] = 1;
                            if (is_null($response->examples)) $response->examples = [];
                            foreach ($response->examples as $type => &$example) {
                                if ('application/json' === $type && $response->response == 200) {
                                    $example['response'] = [
                                        '$ref' => $definition,
                                    ];
                                }
                            }
                        }

                        foreach ($annotation->parameters as &$parameter) {
                            if (!$merged[$controllerObj::ENTITY_CLASS]) {
                                $parameter->_unmerged[] = $model;
                                $merged[$controllerObj::ENTITY_CLASS] = 1;
                            }

                            if ($parameter->schema && $parameter->schema->ref == '#/definitions/SomeObject') {
                                $parameter->schema->ref = $definition;
                            }
                        }
                    }
                }
            }
        }
    }

    public function setModelRegistry(ModelRegistry $modelRegistry)
    {
        $this->modelRegistry = $modelRegistry;
    }
}
