<?php

namespace Webslon\Bundle\ApiBundle\Model\Response;

use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Webslon\Library\Api\Response\BaseItemChildResponse;

/**
 * Class EnumItems
 */
class EnumItems extends BaseItemChildResponse
{
    /**
     * @var string
     * @SWG\Property(type="string", description="Короткое наименование класса", example="OrderHistoryReturnReasonTypeEnum")
     */
    public $shortClassName;

    /**
     * @var string
     * @SWG\Property(type="string", description="Полное наименование класса с namespace", example="App\\DBAL\\Types\\OrderHistoryReturnReasonTypeEnum")
     */
    public $fullClassName;

    /**
     * @var array
     * @SWG\Property(description="Массив enum значений", type="array", @SWG\Items(ref=@Model(type=Item::class)))
     */
    public $items;
}
