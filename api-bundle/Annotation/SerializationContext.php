<?php

namespace Webslon\Bundle\ApiBundle\Annotation;

/**
 * Class SerializationContext
 *
 * @Annotation
 */
class SerializationContext
{
    /** @var string */
    public $code;
}