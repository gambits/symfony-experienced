<?php


namespace Webslon\Bundle\ApiBundle\Service\CRUD\Filters\FilterModel;

/**
 * Class ConstnantFilter
 */
class ConstantFilter
{
    const FIELDS = 'fields';
    const FILTER = 'filter';
    const GROUP = 'group';
    const ORDER = 'order';
    const LIMIT = 'limit';
    const OFFSET = 'offset';
    const DOWNLOAD = 'download';
}
