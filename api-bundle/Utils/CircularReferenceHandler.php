<?php

namespace Webslon\Bundle\ApiBundle\Utils;

/**
 * Class CircularReferenceHandler
 */
class CircularReferenceHandler
{
    /**
     * @param object $object
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        return $object->getId();
    }
}