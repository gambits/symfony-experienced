<?php


namespace App\DTO;

/**
 * Class CacheItemDto
 */
class CacheItemDto
{
    /**
     * @var string
     */
    public $key;
    /**
     * @var mixed
     */
    public $value;
    /**
     * @var string
     */
    public $expireAt;

    /**
     * CacheItemDto constructor.
     *
     * @param string $key
     * @param mixed  $value
     * @param string $expireAt
     */
    public function __construct(string $key, $value, string $expireAt)
    {
        $this->key = $key;
        $this->value = $value;
        $this->expireAt = $expireAt;
    }
}