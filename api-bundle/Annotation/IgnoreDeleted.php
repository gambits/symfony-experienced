<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 26.12.18
 * Time: 19:08
 */

namespace Webslon\Bundle\ApiBundle\Annotation;

/**
 * @Annotation
 * @Target("PROPERTY")
 */
class IgnoreDeleted
{

}