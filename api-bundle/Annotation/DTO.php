<?php

namespace Webslon\Bundle\ApiBundle\Annotation;

/**
 * Class DTO
 * @Annotation
 */
class DTO
{
    /** @var string */
    public $entityClass;
}