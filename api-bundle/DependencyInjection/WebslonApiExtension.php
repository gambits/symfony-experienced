<?php

namespace Webslon\Bundle\ApiBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @see http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class WebslonApiExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('webslon_api.queue_entities_directory', $config['queue_entities_directory'] ?? '');
        $container->setParameter('webslon_api.queue_dto_directory', $config['queue_dto_directory'] ?? '');

        $container->setParameter('webslon_api.rabbitmq.host', $config['rabbitmq']['host'] ?? '127.0.0.1');
        $container->setParameter('webslon_api.rabbitmq.port', $config['rabbitmq']['port'] ?? 5672);
        $container->setParameter('webslon_api.rabbitmq.username', $config['rabbitmq']['username'] ?? 'guest');
        $container->setParameter('webslon_api.rabbitmq.password', $config['rabbitmq']['password'] ?? 'guest');
        $container->setParameter('webslon_api.rabbitmq.vhost', $config['rabbitmq']['vhost'] ?? '/');
        $container->setParameter('webslon_api.rabbitmq.http_port', $config['rabbitmq']['http_port'] ?? '15672');
        $container->setParameter('webslon_api.rabbitmq.protocol', $config['rabbitmq']['http_protocol'] ?? 'http');
        $container->setParameter('webslon_api.rabbitmq.rpc_response_storage', $config['rabbitmq']['rpc_response_storage'] ?? '');
        $container->setParameter('webslon_api.rabbitmq.rpc_response_queue', $config['rabbitmq']['rpc_response_queue'] ?? '');
        $container->setParameter('webslon_api.rabbitmq.rpc_response_frontend_queue_prefix', $config['rabbitmq']['rpc_response_frontend_queue_prefix'] ?? '');
        $container->setParameter('webslon_api.rabbitmq.lifetime_callback_rpc_queue', $config['rabbitmq']['lifetime_callback_rpc_queue'] ?? 30);
        
        if (isset($config['rabbitmq']) && isset($config['rabbitmq']['redis'])) {
            $container->setParameter('webslon_api.rabbitmq.redis.host', $config['rabbitmq']['redis']['host'] ?? '');
            $container->setParameter('webslon_api.rabbitmq.redis.port', $config['rabbitmq']['redis']['port'] ?? '');
            $container->setParameter('webslon_api.rabbitmq.redis.scheme', $config['rabbitmq']['redis']['scheme'] ?? '');
        }
    }
}
