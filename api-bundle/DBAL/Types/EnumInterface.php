<?php

namespace Webslon\Bundle\ApiBundle\DBAL\Types;

/**
 * Interface EnumInterface
 */
interface EnumInterface
{
    /**
     * @return array
     */
    public static function choices(): array;
}
