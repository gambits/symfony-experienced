<?php

namespace Webslon\Bundle\ApiBundle\DTO\Enum;

use Webslon\Bundle\ApiBundle\Annotation\EnumAnnotation;
use Swagger\Annotations as SWG;
use Webslon\Bundle\ApiBundle\DBAL\Types\EnumInterface;
use Webslon\Bundle\ApiBundle\DBAL\Traits\EnumTrait;

/**
 * @EnumAnnotation()
 * Class BaseEnum
 */
abstract class BaseEnum implements EnumInterface
{
    use EnumTrait;

    const NULLABLE = 'NULL';

    /**
     * @var string|null
     * @SWG\Property(type="string", description="Идентификатор значения В_ВЕРХНЕМ_РЕГИСТРЕ")
     */
    public $code;

    /**
     * @var string
     * @SWG\Property(type="string", description="Представление значения")
     */
    public $title;

    /**
     * @param string|null $code
     *
     * @return self|null
     */
    public static function getItem(?string $code): ?self
    {
        if (static::hasCode($code)) {
            return (new static)->setCode($code);
        }

        return null;
    }
}
