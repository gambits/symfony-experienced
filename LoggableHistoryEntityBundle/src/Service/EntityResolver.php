<?php

namespace Webslon\Bundle\WebslonEntityHistory\Service;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Webslon\Bundle\WebslonEntityHistory\Annotation\History;

/**
 * Class EntityResolver
 */
class EntityResolver
{
    /**
     * @var AnnotationReader
     */
    private $reader;

    /**
     * @var
     */
    private $em;

    /**
     * EntityResolver constructor.
     *
     * @param AnnotationReader $reader
     */
    public function __construct(AnnotationReader $reader, EntityManagerInterface $em)
    {
        $this->reader = $reader;
        $this->em = $em;
    }

    /**
     * @param string $alias
     *
     * @return History|null
     * @throws \ReflectionException
     */
    public function resolve($alias):?History
    {
        $historyAnnotation = null;
        foreach ($this->em->getMetadataFactory()->getAllMetadata() as $classMetadata) {
            if ($historyAnnotation = $this->reader->getClassAnnotation(new \ReflectionClass($classMetadata->getName()), History::class)) {
                /** @var History $historyAnnotation */
                if ($historyAnnotation->alias === $alias) {
                    $historyAnnotation->entity = $classMetadata->getName();
                    break;
                }
            }
        }

        return $historyAnnotation;
    }
    /**
     * @param string $alias
     *
     * @return string|null
     * @throws \ReflectionException
     */
    public function resolveEntity($alias):?string
    {
        $entityNamespace = null;
        foreach ($this->em->getMetadataFactory()->getAllMetadata() as $classMetadata) {
            if ($historyAnnotation = $this->reader->getClassAnnotation(new \ReflectionClass($classMetadata->getName()), History::class)) {
                /** @var History $historyAnnotation */
                if ($historyAnnotation->alias === $alias) {
                    $entityNamespace = $classMetadata->getName();
                    break;
                }
            }
        }

        return $entityNamespace;
    }
    /**
     * @param object|string $entity
     *
     * @return History
     * @throws \ReflectionException
     */
    public function getData($entity)
    {
        $entity = is_object($entity) ? get_class($entity) : $entity;

        return $this->reader->getClassAnnotation(new \ReflectionClass($entity), History::class);
    }
}