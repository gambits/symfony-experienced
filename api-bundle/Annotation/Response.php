<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 3/25/18
 * Time: 1:38 AM
 */

namespace Webslon\Bundle\ApiBundle\Annotation;


use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @Annotation
 *
 * Class Response
 */
class Response extends \Swagger\Annotations\Response
{
    const DEFAULT_LIST_RESPONSE = 'list';
    const DEFAULT_ITEM_RESPONSE = 'item';
    const DEFAULT_UPDATE_RESPONSE = 'update';
    const DEFAULT_DELETE_RESPONSE = 'update';

    const DEFAULT_ERROR_UPDATE_RESPONSE = 'error.update';
    const DEFAULT_ERROR_CREATE_RESPONSE = 'error.create';
    const DEFAULT_ERROR_DELETE_RESPONSE = 'error.delete';


    /**
     * Identifer for unique response and overriding default reponses
     *
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $model;
}