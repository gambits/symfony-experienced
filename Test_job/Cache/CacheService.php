<?php


namespace App\Cache;

use App\DTO\CacheItemDto;
use App\Interfaces\CacheDataInterface;

/**
 * Class CacheService
 */
class CacheService implements CacheDataInterface
{
    /**
     * @param string $key
     */
    public function get($key)
    {
    }

    /**
     * @param CacheItemDto $itemDto
     */
    public function set(CacheItemDto $itemDto)
    {
    }

    /**
     * @param CacheItemDto $itemDto
     *
     * @return mixed|void
     */
    public function invalidateByKey(CacheItemDto $itemDto)
    {
    }

    /**
     * @return mixed|void
     */
    public function invalidateAll()
    {
    }
}