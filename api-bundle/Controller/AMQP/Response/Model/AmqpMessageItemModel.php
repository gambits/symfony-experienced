<?php


namespace Webslon\Bundle\ApiBundle\Controller\AMQP\Response\Model;

use Swagger\Annotations as SWG;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class AmqpMessageItemModel
 */
class AmqpMessageItemModel
{
    /**
     * @var integer
     * @Groups({"default"})
     * @SWG\Property(type="integer", description="Вес сообщения в байтах")
     */
    public $payload_bytes;
    /**
     * @var bool
     * @Groups({"default"})
     * @SWG\Property(type="boolean", description="Повторная отправка")
     */
    public $redelivered;
    /**
     * @var string
     * @Groups({"default"})
     * @SWG\Property(type="string", description="Наименование обменника")
     */
    public $exchange;

    /**
     * @var string
     * @Groups({"default"})
     * @SWG\Property(type="string", description="Ключ маршрутизации сообщения")
     */
    public $routing_key;
    /**
     * @var integer
     * @Groups({"default"})
     * @SWG\Property(type="integer", description="Кол-во сообщений в очереди")
     */
    public $message_count;
    /**
     * @var array
     * @Groups({"default"})
     * @SWG\Property(type="", description="Свойства сообщения")
     */
    public $properties;

    /**
     * @var array
     * @Groups({"default"})
     * @SWG\Property(type="", description="Тело сообщения")
     */
    public $payload;
}