<?php

namespace Webslon\Bundle\ApiBundle\Helper;

class SetterHelper
{
    public static function getSetter(string $property): string
    {
        return 'set' . ucfirst($property);
    }
}
