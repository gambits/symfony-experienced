<?php

namespace Webslon\Bundle\ApiBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Translation\Translator;
use Webslon\Bundle\ApiBundle\Service\DTO\DTOFactory;
use Webslon\Library\Serializer\Service\DeserializeService;
use Webslon\Library\Api\Response\BaseResponse as ApiResponse;

/**
 * Interface DependenciesInterface
 */
interface DependenciesInterface
{
    /**
     * @return EntityManager|EntityManagerInterface
     */
    public function getOm();

    /**
     * @return Serializer
     */
    public function getSerializer();

    /**
     * @return ApiResponse
     */
    public function getResponse();

    /**
     * @return ValidationService
     */
    public function getValidator();

    /**
     * @return EventDispatcher
     */
    public function getDispatcher();

    /**
     * Method defined in trait SaveEntityTrait
     *
     * @param object $entity
     */
    public function saveEntity($entity);

    /**
     * @param mixed $entity
     *
     * @return mixed
     */
    public function removeEntity($entity);

    /**
     * @return RequestStack
     */
    public function getRequest();

    /**
     * @return Translator
     */
    public function getTranslator();

    /**
     * @return DTOFactory
     */
    public function getDtoFactory(): DTOFactory;
}
