<?php

namespace Webslon\Bundle\ApiBundle\Controller\CRUD;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Webslon\Bundle\ApiBundle\Annotation\Operation;
use Webslon\Bundle\ApiBundle\Controller\ApiControllerInterface;
use Webslon\Bundle\ApiBundle\Service\CRUD\AddItemService;
use Webslon\Bundle\ApiBundle\Service\SerializationContextFetcher;
use Webslon\Bundle\ApiBundle\Service\ValidationService;
use Webslon\Library\Api\Exception\ApiException;

/**
 * Trait AddItemTrait
 */
trait AddItemTrait
{
    /**
     * @Route("/", methods={"POST"})
     * @Operation("post")
     * @SWG\Post(
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="entity",
     *          in="body",
     *          description="Json-объект с данными добавляемой сущности",
     *          schema={}
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Успешный ответ сервиса, в результате приходят данные добаленного объекта", examples={
     *              "application/json":{
     *                  "status": true,
     *                  "response": {"id": 1},
     *                  "errors": null
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="Ошибка выполнения операции",
     *         examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": null,
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode":
     *     "ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     )
     * )
     * @param Request        $request
     * @param AddItemService $service
     *
     * @return Response
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws ApiException
     */
    public function addItemAction(Request $request, AddItemService $service, SerializationContextFetcher $serializationContextFetcher): Response
    {
        if ($this instanceof ApiControllerInterface) {
            $serializationContext = $this->getSerializationContext('addItem');
        } else {
            $serializationContext = [];
        }

        $entityClass = $this instanceof DynamicEntityClassControllerInterface ? $this->getEntityClass() : self::ENTITY_CLASS;

        $isDefinedDtoClass = defined(get_class($this) . '::DTO_CLASS');
        $class = $isDefinedDtoClass ? self::DTO_CLASS : $entityClass;
        if ($groups = $serializationContextFetcher->getSerializationGroups('post', $class)) {
            if (!isset($serializationContext['groups'])) {
                $serializationContext['groups'] = [];
            }

            $serializationContext['groups'] = array_merge($serializationContext['groups'], $groups);
        }

        if ($isDefinedDtoClass) {
            $service->setDtoClass(self::DTO_CLASS);
        }

        $result = $service->add($request->getContent(), $entityClass, [ValidationService::GROUP_DEFAULT, ValidationService::GROUP_CREATE], $serializationContext, $serializationContext);

        return $result->send();
    }
}
