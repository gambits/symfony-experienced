<?php


namespace Webslon\Bundle\ApiBundle\Response\Async;


use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Webslon\Library\Api\Response\BaseResponse;

class AsyncResponse extends BaseResponse
{
    /**
     * @var boolean
     *
     * @SWG\Property(type="boolean", default="true", description="Признак успешного ответа сервиса")
     */
    public $status;

    /**
     * @var AsyncResponseBody
     *
     * @SWG\Property(type="object", ref=@Model(type=AsyncResponseBody::class), description="Результат успешной работы сервиса, в случае status=true.")
     */
    public $response;
}
