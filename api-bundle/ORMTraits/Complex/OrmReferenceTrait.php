<?php
/**
 * Created by PhpStorm.
 * User: viktorkrasnov
 * Date: 20.08.17
 * Time: 15:18
 */

namespace Webslon\Bundle\ApiBundle\ORMTraits\Complex;

use Swagger\Annotations as SWG; // не удалять - почему-то не работают вложенные трейты
use Webslon\Bundle\ApiBundle\ORMTraits\ {
    OrmIdTrait,
    OrmNameTrait,
    OrmDeletedTrait
};

trait OrmReferenceTrait {

    use OrmIdTrait, OrmNameTrait, OrmDeletedTrait;

    /**
     * @param $sourceObject
     */
    public function fill($sourceObject) {
        foreach ($sourceObject as $key=>$value){
            if($key != "id" && !is_null($sourceObject->$key) && (property_exists($this, $key))) {
                $this->$key = $sourceObject->$key;
            }
        }
    }
}
