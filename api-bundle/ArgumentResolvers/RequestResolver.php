<?php

namespace Webslon\Bundle\ApiBundle\ArgumentResolvers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Webslon\Bundle\ApiBundle\Request\Request as SupportRequest;
use Webslon\Library\Api\Exception\ApiException;
use Webslon\Library\Api\Exception\ValidationExceptionCollection;

final class RequestResolver implements ArgumentValueResolverInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return bool
     * @throws \ReflectionException
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        if (null !== $argument->getType() && class_exists($argument->getType())) {
            $r = new \ReflectionClass($argument->getType());
            if ($r->implementsInterface(SupportRequest::class)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     * @return \Generator
     * @throws ValidationExceptionCollection
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): \Generator {
        $object = $this->serializer->deserialize(
            $request->getContent(),
            $argument->getType(),
            'json'
        );

        $errors = $this->validator->validate($object);

        if (0 < \count($errors)) {
            $exceptions = [];

            /* @var $error ConstraintViolation */
            foreach ($errors as $error) {
                $exceptions[] = new ApiException(
                    $error->getMessage(),
                    'VALIDATION_ERROR',
                    $error->getPropertyPath()
                );
            }

            throw new ValidationExceptionCollection($exceptions);
        }

        yield $object;
    }
}
