<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 22.01.19
 * Time: 15:59
 */

namespace Webslon\Bundle\ApiBundle\Validator\Constraint;

use Webslon\Bundle\ApiBundle\Validator\UniqueEntityValidator;

/**
 * Class UniqueEntity
 * @package Webslon\Bundle\ApiBundle\Validator\Constraint
 *
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class UniqueEntity extends \Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity
{
    public $service = UniqueEntityValidator::class;
}