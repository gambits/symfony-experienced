<?php

namespace Webslon\Bundle\ApiBundle\Controller\CRUD;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Webslon\Bundle\ApiBundle\Annotation\Operation;
use Webslon\Bundle\ApiBundle\Controller\ApiControllerInterface;
use Webslon\Bundle\ApiBundle\Service\CRUD\GetItemService;
use Webslon\Bundle\ApiBundle\Service\SerializationContextFetcher;
use Webslon\Library\Api\Exception\ApiException;
use Webslon\Bundle\ApiBundle\Response\Async\AsyncResponse;

/**
 * Trait GetItemTrait
 */
trait GetItemTrait
{
    /**
     * @Route("/{id}/", requirements={"id": "[a-z0-9\-\w+]{1,}"}, methods={"GET"})
     * @Operation("get")
     * @SWG\Get(
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="Идентификатор сущности",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="fields",
     *          in="query",
     *          type="string",
     *          description="Json-объект с списком полей необходимых для получения"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Успешный ответ сервиса, в результате приходят
     *     данные запрошенного объекта", examples={
     *              "application/json":{
     *                  "status": true,
     *                  "response": {"id": 1},
     *                  "errors": null
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     ),
     *     @SWG\Response(
     *         response=202,
     *         description="Запрошенный объект не найден, но запрос на его создание принят в обработку.",
     *         examples={
     *              "application/json":{
     *                  "status": true,
     *                  "response": { "corellationId" : "259d7c60-e4d8-4c29-93fe-8947fdb05dd1" },
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode":"ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=AsyncResponse::class)
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Entity not found", examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": null,
     *                  "errors": {
     *                  "errors": {0: {"message": "Entity not found", "stringCode": "ERROR_404", "relatedField": null}}
     *                  }
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="Ошибка выполнения операции",
     *         examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": null,
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode":
     *     "ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     )
     * )
     * @param string         $id
     * @param Request        $request
     * @param GetItemService $service
     *
     * @return Response
     * @throws \InvalidArgumentException
     * @throws ApiException
     */
    public function getItemAction(string $id, Request $request, GetItemService $service, SerializationContextFetcher $serializationContextFetcher): Response
    {
        if ($this instanceof ApiControllerInterface) {
            $serializationContext = $this->getSerializationContext('getItem');
        } else {
            $serializationContext = [];
        }

        $entityClass = $this instanceof DynamicEntityClassControllerInterface ? $this->getEntityClass() : self::ENTITY_CLASS;

        $isDefinedDtoClass = defined(get_class($this).'::DTO_CLASS');
        if ($groups = $serializationContextFetcher->getSerializationGroups('detail', $isDefinedDtoClass ? self::DTO_CLASS : $entityClass)) {
            if (!isset($serializationContext['groups'])) {
                $serializationContext['groups'] = [];
            }

            $serializationContext['groups'] = array_merge($serializationContext['groups'], $groups);
        }

        if ($isDefinedDtoClass) {
            $service->setDtoClass(self::DTO_CLASS);
        }

        $result = $service->getItem($id, $entityClass, $request->get('fields'), $serializationContext);
        
        return $result->send();
    }
}
