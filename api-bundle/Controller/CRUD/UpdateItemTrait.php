<?php

namespace Webslon\Bundle\ApiBundle\Controller\CRUD;

use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Webslon\Bundle\ApiBundle\Annotation\Operation;
use Webslon\Bundle\ApiBundle\Controller\ApiControllerInterface;
use Webslon\Bundle\ApiBundle\Service\CRUD\UpdateItemService;
use Webslon\Bundle\ApiBundle\Service\SerializationContextFetcher;
use Webslon\Bundle\ApiBundle\Service\ValidationService;
use Webslon\Library\Api\Exception\ApiException;

/**
 * Trait UpdateItemTrait
 */
trait UpdateItemTrait
{
    /**
     * @Route("/{id}/", requirements={"id": "[a-z0-9\-\w+]{1,}"}, methods={"PUT"})
     * @Operation("put")
     * @SWG\Put(
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="Идентификатор обновляемой сущности",
     *          type="string",
     *     ),
     *     @SWG\Parameter(
     *          name="entity",
     *          in="body",
     *          description="Json-объект с данными обновляемой сущности, допустима передача сущности частично",
     *          schema={}
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Успешный ответ сервиса, в результате приходят
     *     данные обновленного объекта",
     *          examples={
     *              "application/json":{
     *                  "status": true,
     *                  "response": {"id": 1},
     *                  "errors": null
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="Ошибка операции обновления",
     *         examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": null,
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode": "ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     )
     * )
     *
     * @param string            $id
     * @param Request           $request
     * @param UpdateItemService $service
     *
     * @return Response
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws ApiException
     */
    public function updateItemAction(string $id, Request $request, UpdateItemService $service, SerializationContextFetcher $serializationContextFetcher): Response
    {
        if ($this instanceof ApiControllerInterface) {
            $serializationContext = $this->getSerializationContext('updateItem');
        } else {
            $serializationContext = [];
        }

        $entityClass = $this instanceof DynamicEntityClassControllerInterface ? $this->getEntityClass() : self::ENTITY_CLASS;

        $isDefinedDtoClass = defined(get_class($this) . '::DTO_CLASS');
        if ($groups = $serializationContextFetcher->getSerializationGroups('put', $isDefinedDtoClass ? self::DTO_CLASS : $entityClass)) {
            if (!isset($serializationContext['groups'])) {
                $serializationContext['groups'] = [];
            }

            $serializationContext['groups'] = array_merge($serializationContext['groups'], $groups);
        }

        $outputSerializationContext = $serializationContext;
        $outputSerializationContext['groups'] = $serializationContext['groups'] ?? [];
        $outputSerializationContext['groups'][] = 'detail';

        if ($isDefinedDtoClass) {
            $service->setDtoClass(self::DTO_CLASS);
        }

        $result = $service->update($id, $request->getContent(), $entityClass, [ValidationService::GROUP_UPDATE_PUT, ValidationService::GROUP_DEFAULT], $outputSerializationContext, $serializationContext);

        return $result->send();
    }
}