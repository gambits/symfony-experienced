<?php

namespace Webslon\Bundle\ApiBundle\Service;

use Webslon\Bundle\ApiBundle\EventDispatcher\EventRequest;

/**
 * Interface ApiServiceInterface
 */
interface ApiServiceInterface
{
    /**
     * @param mixed $data
     * @param array $eventsList
     *
     * @return EventRequest
     */
    public function generateEvent($data, array $eventsList = []): EventRequest;
}
