<?php

namespace Webslon\Bundle\ApiBundle\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target("CLASS")
 */
class EnumAnnotation
{
}
