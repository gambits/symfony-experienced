<?php


namespace Webslon\Bundle\ApiBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * Class ConstraintEnum
 */
class ConstraintEnum extends Constraint
{
    public $message = 'error.validation.enum';

    /**
     * @inheritDoc
     * @return array|string
     */
    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
