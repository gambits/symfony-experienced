<?php

namespace Webslon\Bundle\ApiBundle\Model\Response;

use Symfony\Component\HttpFoundation\Response;
use Webslon\Library\Api\Response\BaseItemChildResponse;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Webslon\Library\Api\Exception\ApiException;

/**
 * Class EnumListResponse
 */
class EnumListResponse
{
    /**
     * @var boolean
     * @SWG\Property(type="boolean", description="Признак успешного ответа сервиса")
     */
    public $status = true;

    /**
     * @var integer
     * @SWG\Property(type="integer")
     */
    public $httpResponseCode = Response::HTTP_OK;

    /**
     * @var array|null
     * @SWG\Property(type="array", description="Массив ошибок, в случае status=false", @SWG\Items(ref=@Model(type=ApiException::class)))
     */
    public $errors;

    /**
     * @var EnumItems
     * @SWG\Property(type="array",  @SWG\Items(ref=@Model(type=EnumItems::class)), description="Многомерный массив с характеристиками перечислений")
     */
    public $response;

    /**
     * @var string
     * @SWG\Property(type="string", description="Идентификатор запроса для систем логирования")
     */
    public $requestId;
}
