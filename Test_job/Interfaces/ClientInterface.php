<?php


namespace App\Interfaces;

/**
 * Interface ClientInterface
 */
interface ClientInterface
{
    /**
     * @param array $params
     *
     * @return mixed
     */
    public function get(array $params);

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function post(array $params);

    /**
     * @param int|string $id
     * @param array      $params
     *
     * @return mixed
     */
    public function patch($id, array $params);

    /**
     * @param int|string $id
     * @param array      $params
     *
     * @return mixed
     */
    public function delete($id, array $params);
}