<?php


namespace Webslon\Bundle\ApiBundle\Exception;

use Webslon\Library\Api\Exception\ApiException;

/**
 * Class AmqpEmptyAnnotationException
 */
class AmqpEmptyBatchAnnotationException extends ApiException
{
    public $message = 'error.amqp.entity.empty.batch_annotation';
}