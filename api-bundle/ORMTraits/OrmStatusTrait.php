<?php

namespace Webslon\Bundle\ApiBundle\ORMTraits;

use Swagger\Annotations as SWG;

trait OrmStatusTrait
{
    /**
     * @Symfony\Component\Serializer\Annotation\Groups({
     *     "default"
     * })
     *
     * @var integer
     * @ORM\Column(type="integer")
     * @SWG\Property(description="Статус", type="integer")
     */
    private $status;

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }
}
