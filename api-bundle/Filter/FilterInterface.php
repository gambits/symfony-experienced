<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 10.09.18
 * Time: 12:05
 */

namespace Webslon\Bundle\ApiBundle\Filter;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface FilterInterface
 */
interface FilterInterface
{
    /**
     * @param string $entityClass
     * @param array $filtersRequest
     * @return bool
     */
    public function support(string $entityClass, array $filtersRequest) : bool;

    /**
     * @param QueryBuilder $queryBuilder
     * @param string $entityClass
     * @param string $mainAlias
     * @param array $filtersRequest
     */
    public function handle(QueryBuilder $queryBuilder, string $entityClass, string $mainAlias, array &$filtersRequest);
}