<?php

namespace Webslon\Bundle\ApiBundle\Controller\CRUD;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\Routing\Annotation\Route;
use Webslon\Bundle\ApiBundle\Annotation\Operation;
use Webslon\Bundle\ApiBundle\Controller\ApiControllerInterface;
use Webslon\Bundle\ApiBundle\Service\CRUD\GetListService;
use Webslon\Bundle\ApiBundle\Service\SerializationContextFetcher;
use Webslon\Library\Api\Exception\ApiException;
use Webslon\Bundle\ApiBundle\Response\Async\AsyncResponse;

/**
 * Trait GetListTrait
 */
trait GetListTrait
{
    /**
     * @Route("/", methods={"GET"})
     * @Operation("list")
     * @SWG\Get(
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="search",
     *          in="query",
     *          type="string",
     *          description="Строка для поиска по всем полям сущности типа string и text"
     *     ),
     *     @SWG\Parameter(
     *          name="filter",
     *          in="query",
     *          type="string",
     *          description="Json-объект с фильтром записей вида {&quot;column_name&quot;: &quot;value&quot;}
 * Перед column_name можно указывать один из операторов: =, !=, &gt;, &gt;=, &lt;, &lt;=.
 * Без указания оператора используется сравнение LIKE.
 * Поле value может быть массивом, в этом случае происходит проверка на вхождение значения в массив. Если указать оператор ! перед column_name - то проверка будет на НЕ вхождение значения в массив.
 * При передаче нескольких условий они отрабатывают по И. Для изменения логики в фильтре надо передать значение OR с ключем LOGIC &quot;LOGIC&quot;: &quot;OR&quot;.

Пример сложного фильтра:
{&quot;LOGIC&quot;: &quot;OR&quot;, &quot;0&quot;: {&quot;!id&quot;: 1, &quot;deleted&quot;: true}, &quot;1&quot;: {&quot;=id&quot;: 10}}
"
     *     ),
     *     @SWG\Parameter(
     *          name="fields",
     *          in="query",
     *          type="string",
     *          description="Json-объект со списком отбираемых полей, которые надо вывести в ответе (по умолчанию все)
Пример:
{&quot;0&quot;:&quot;id&quot;,&quot;1&quot;:&quot;number&quot;,&quot;status&quot;:[&quot;id&quot;,&quot;name&quot;,&quot;code&quot;]}
"
     *     ),
     *      @SWG\Parameter(
     *          name="order",
     *          in="query",
     *          type="string",
     *          description="Json-объект с сортировкой записей вида {column_name: &quot;DESС&quot;}"
     *     ),
     *      @SWG\Parameter(
     *          name="offset",
     *          in="query",
     *          type="integer",
     *          description="Начальная запись в списке",
     *          default=0
     *     ),
     *      @SWG\Parameter(
     *          name="limit",
     *          in="query",
     *          type="integer",
     *          description="Число записей в списке",
     *          default=20
     *     ),
     *      @SWG\Parameter(
     *          name="download",
     *          in="query",
     *          type="string",
     *          description="Режим скачивания списка как файл. Если передать значение xls - произойдет скачивание файла в формате MS Excel, при этом параметры offset и limit будут проигнорированы (будет скачан весь список).",
     *          enum={"xls"}
     *     ),
     *      @SWG\Response(
     *         response=200,
     *         description="Успешный ответ сервиса",
     *         examples={
     *              "application/json":{
     *                  "status": true,
     *                  "response": {"items": {}, "totalCount": 100},
     *                  "errors": null
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     ),
     *     @SWG\Response(
     *         response=202,
     *         description="Запрошенный объект не найден, но запрос на его создание принят в обработку.",
     *         examples={
     *              "application/json":{
     *                  "status": true,
     *                  "response": { "corellationId" : "259d7c60-e4d8-4c29-93fe-8947fdb05dd1" },
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode":"ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=AsyncResponse::class)
     *     ),
     *      @SWG\Response(
     *         response="default",
     *         description="Ошибка выполнения операции",
     *         examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": null,
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode": "ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     )
     * )
     *
     * @param RequestStack                $request
     * @param GetListService              $service
     * @param SerializationContextFetcher $serializationContextFetcher
     *
     * @return Response
     * @throws ApiException
     * @throws \Doctrine\ORM\Mapping\MappingException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \ReflectionException
     */
    public function getListAction(RequestStack $request, GetListService $service, SerializationContextFetcher $serializationContextFetcher): Response
    {
        if ($this instanceof ApiControllerInterface) {
            $serializationContext = $this->getSerializationContext('getList');
        } else {
            $serializationContext = [];
        }

        $entityClass = $this instanceof DynamicEntityClassControllerInterface ? $this->getEntityClass() : self::ENTITY_CLASS;

        $usageDoctrinePaginator = defined(get_class($this).'::USAGE_DOCTRINE_PAGINATOR') && self::USAGE_DOCTRINE_PAGINATOR;
        $isDefinedDtoClass = defined(get_class($this).'::DTO_CLASS');
        if ($groups = $serializationContextFetcher->getSerializationGroups('list', $isDefinedDtoClass ? self::DTO_CLASS : $entityClass)) {
            if (!isset($serializationContext['groups'])) {
                $serializationContext['groups'] = [];
            }

            $serializationContext['groups'] = array_merge($serializationContext['groups'], $groups);
        }

        if ($isDefinedDtoClass) {
            $service->setDtoClass(self::DTO_CLASS);
        }

        $result = $service->getList($request->getCurrentRequest(), $entityClass, $serializationContext, $usageDoctrinePaginator);

        return $result->send();
    }
}
