<?php


namespace Webslon\Bundle\ApiBundle\DependencyInjection\Compiler;

use Doctrine\Common\Annotations\Reader;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Finder\Finder;
use Webslon\Bundle\ApiBundle\AMQP\PlaceholderResolver;
use Webslon\Bundle\ApiBundle\AMQP\Router\Route;
use Webslon\Bundle\ApiBundle\AMQP\Router\RouterCollection;
use Webslon\Bundle\ApiBundle\Annotation\Enqueue\Consume;
use Webslon\Bundle\ApiBundle\Annotation\Enqueue\CrudBatchConsume;
use Webslon\Bundle\ApiBundle\Annotation\Enqueue\CrudConsume;
use Webslon\Bundle\ApiBundle\Annotation\Enqueue\Produce;
use Webslon\Bundle\ApiBundle\Consumer\Batch\CreateConsumer;
use Webslon\Bundle\ApiBundle\Consumer\Batch\DeleteConsumer;
use Webslon\Bundle\ApiBundle\Consumer\Batch\ReplaceConsumer;
use Webslon\Bundle\ApiBundle\Consumer\Batch\UpdateConsumer;

/**
 * Class EnqueueSubscriberPass
 */
final class EnqueueBatchSubscriberPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     *
     * @throws \ReflectionException
     */
    public function process(ContainerBuilder $container)
    {
        $annotationReader = $container->get(Reader::class);
        $entityDirectories = $container->getParameter('webslon_api.queue_entities_directory');
        $dtoDirectories = $container->getParameter('webslon_api.queue_dto_directory');
        $sourceDirectory = $container->getParameter('kernel.root_dir').'/../';

        $directories = array_map(function($directory) use ($sourceDirectory) {
            return $sourceDirectory.$directory;
        }, array_merge($dtoDirectories, $entityDirectories));


        $phpFiles = Finder::create()->files()->in($directories)->name('*.php');

        $classes = [];
        foreach ($phpFiles as $phpFile) {
            $className = str_replace('.php', '', $phpFile->getFileName());
            $classes[] =  $this->getFullNamespace($phpFile->getRealpath()).'\\'.$className;
        }

        $routeCollectionServiceId = RouterCollection::class;
        $routeCollectionDefinition = $container->getDefinition($routeCollectionServiceId);

        $routes = [];
        $prefixToHandler = [
            Route::TOPIC_CREATE_PREFIX => CreateConsumer::class,
            Route::TOPIC_DELETE_PREFIX => DeleteConsumer::class,
            Route::TOPIC_UPDATE_PREFIX => UpdateConsumer::class,
            Route::TOPIC_REPLACE_PREFIX => ReplaceConsumer::class,
        ];
        foreach ($classes as $class) {
            $reflectionClass = new \ReflectionClass($class);
            /** @var CrudConsume $queueConsume */
            if (!$queueConsume = $annotationReader->getClassAnnotation($reflectionClass, CrudBatchConsume::class)) {
                continue;
            }

            foreach ($prefixToHandler as $prefix => $handler) {
                if (!isset($queueConsume->topicsMap[$prefix])) {
                    continue;
                }

                $consume = $queueConsume->topicsMap[$prefix];
                if (!$consume instanceof Consume) {
                    throw new \RuntimeException(sprintf('Topic map value must be string or %s got %s', Consume::class, gettype($consume)));
                }
                /** @var Produce $onErrors */
                list($queue, $name, $exchange, $exchangeBindKey, $onErrors) = [
                    $consume->queue,
                    $consume->queue,
                    $consume->exchangeName,
                    $consume->exchangeBindKey,
                    $consume->onErrors
                ];

                $placeholderResolver = new PlaceholderResolver();
                list($queue, $name, $exchange, $exchangeBindKey) = $placeholderResolver->handlePlaceholdersParametersFromContainer($container, $queue, $name, $exchange, $exchangeBindKey);

                $serviceId = sprintf('webslon.consumer.%s', $name);

                $container->register($serviceId, $handler)
                    ->setPublic(true)
                    ->addArgument($reflectionClass->getName())
                    ->setAutoconfigured(true)
                    ->setAutowired(true)
                ;

                $routes[] = array_merge(Route::createFromConsume($consume, $serviceId, 'process'), [
                    'name' => $name,
                    'queue' => $queue,
                    'onErrors' => $onErrors ? [
                        'routingKey' => $onErrors->routingKey,
                        'exchange' => $onErrors->exchange,
                        'queue' => $onErrors->queue,
                    ] : null,
                    'itemType' => $reflectionClass->getName(),
                    'consumer' => $serviceId,
                    'action' => 'process',
                    'exchange_bind_key' => $exchangeBindKey,
                    'exchange_name' => $exchange,
                ]);
            }
        }

        $routeCollectionDefinition->setArgument(0, $routes);
    }

    private function getFullNamespace($phpFile)
    {
        $lines = preg_grep('/^namespace /', file($phpFile));
        $namespaceLine = array_shift($lines);

        return trim(str_replace(['namespace',';'], '', $namespaceLine));
    }
}
