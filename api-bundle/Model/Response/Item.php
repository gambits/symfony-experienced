<?php

namespace Webslon\Bundle\ApiBundle\Model\Response;

use Swagger\Annotations as SWG;

/**
 * Class Item
 */
class Item
{
    /**
     * @var string
     * @SWG\Property(type="string", description="Кодовое обозначение", example="STOCK_INCORRECT")
     */
    public $id;

    /**
     * @var string
     * @SWG\Property(type="string", description="Перевод на языке системы", example="Некорректный сток")
     */
    public $name;
}
