<?php

use App\DTO\CacheItemDto;
use App\Interfaces\CacheDataInterface;
use \DateTime;
use \Exception;
use Psr\Log\LoggerInterface;
use App\Interfaces\ClientInterface;

/**
 * Class ConcreteProvider
 */
class ConcreteProvider
{
    /**
     * @var ClientInterface
     */
    public $client;
    /**
     * @var CacheDataInterface
     */
    public $cache;
    /**
     * @var string
     */
    public $cacheLifeTime = '1 day';
    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * ConcreteProvider constructor.
     *
     * @param ClientInterface    $client
     * @param CacheDataInterface $cache
     * @param LoggerInterface    $logger
     */
    public function __construct(ClientInterface $client, CacheDataInterface $cache, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->cache = $cache;
        $this->logger = $logger;
    }

    /**
     * @param array $input
     *
     * @return array|null
     * @throws Throwable
     */
    public function getResponse(array $input): ?array
    {
        $key = $this->getCacheKey($input);
        if ($result = $this->cache->get($key)) {
            return $result;
        }
        try {
            if ($result = $this->client->get($input)) {
                $this->cache->set(new CacheItemDto($key, $result, (new DateTime())->modify($this->cacheLifeTime)));
            }

            return $result;
        } catch (Throwable $e) {
            $this->logger->critical($e->getMessage());
            throw $e;
        }
    }

    /**
     * @param array $input
     *
     * @return string
     */
    public function getCacheKey(array $input): string
    {
        return json_encode($input);
    }
}