<?php

namespace Webslon\Bundle\ApiBundle\Controller\CRUD\Batch;

use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Webslon\Bundle\ApiBundle\Annotation\Operation;
use Webslon\Bundle\ApiBundle\Response\Async\AsyncResponseBody;
use Webslon\Bundle\ApiBundle\Service\CRUD\Interfaces\CrudBatchServiceInterface;
use Webslon\Library\Api\Response\BaseResponse;
/**
 * Trait DeleteItemTrait
 */
trait DeleteBatchTrait
{
    /**
     * @Route("/batch/delete", methods={"DELETE"})
     * @Operation("batch-delete")
     * @SWG\Delete(
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="package",
     *          in="body",
     *          description="Массив с Json-объектами сущностей",
     *          schema={}
     *     ),
     *     @SWG\Response(
     *         response=202,
     *         description="Запрос принят в обработку", examples={
     *              "application/json":{
     *                  "status": true,
     *                  "response": {"corellationId": "d214b5cc-b41b-4755-b110-d1d11dd9d901"},
     *                  "errors": null
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="Ошибка операции обновления",
     *         examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": null,
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode": "ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     )
     * )
     *
     * @param Request                   $request
     * @param CrudBatchServiceInterface $batchService
     * @param BaseResponse              $response
     * @param array                     $params
     *
     * @return Response
     */
    public function deleteBatchAction(Request $request, CrudBatchServiceInterface $batchService, BaseResponse $response, array $params = []): Response
    {
        $dtoName = defined(get_class($this) . '::DTO_CLASS') ? self::DTO_CLASS : null;
        $correlationId = $batchService->handleDelete($request->getContent(), self::ENTITY_CLASS, $dtoName, $params);

        return $response
            ->setHttpResponseCode(Response::HTTP_ACCEPTED)
            ->setResponse((new AsyncResponseBody())->setCorrelationId($correlationId))
            ->send();
    }
}