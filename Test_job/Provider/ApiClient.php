<?php


namespace App\Provider;

use App\Interfaces\ClientInterface;

/**
 * Class ApiClient
 */
class ApiClient implements ClientInterface
{
    /**
     * @var string
     */
    private $host;
    /**
     * @var string
     */
    private $user;
    /**
     * @var string
     */
    private $password;

    /**
     * @param string $host
     * @param string $user
     * @param string $password
     */
    public function __construct(string $host, string $user, string $password)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * @param array $params
     *
     * @return mixed|void
     */
    public function get(array $params)
    {
        // TODO: Implement get() method.
    }

    /**
     * @param array $params
     *
     * @return mixed|void
     */
    public function post(array $params)
    {
        // TODO: Implement post() method.
    }

    /**
     * @param int|string $id
     * @param array      $params
     *
     * @return mixed|void
     */
    public function patch($id, array $params)
    {
        // TODO: Implement patch() method.
    }

    /**
     * @param int|string $id
     * @param array      $params
     *
     * @return mixed|void
     */
    public function delete($id, array $params)
    {
        // TODO: Implement delete() method.
    }
}