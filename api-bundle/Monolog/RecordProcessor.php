<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 12.09.18
 * Time: 0:20
 */

namespace Webslon\Bundle\ApiBundle\Monolog;


class RecordProcessor
{
    public function __invoke(array $record)
    {
        if (isset($record['context']['tags'])) {
            $record['extra']['tags'] = $record['context']['tags'];
        }

        if (isset($record['context']['extra'])) {
            $record['extra'] = $record['context']['extra'];
            unset($record['context']['extra']);
        }

        return $record;
    }
}