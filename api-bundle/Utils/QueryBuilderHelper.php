<?php

namespace Webslon\Bundle\ApiBundle\Utils;
use Doctrine\ORM\QueryBuilder;

/**
 * Class QueryBuilderHelper
 */
class QueryBuilderHelper
{
    public static function showQuery(QueryBuilder $query)
    {
        // define vars
        $output    = NULL;
        $out_query = $query->getQuery()->getSql();
        $out_param = $query->getQuery()->getParameters();
        $len = strlen($out_query);
        $j=0;
        // replace params
        for($i=0; $i < $len; $i++) {
            try {
                $output .= ( strpos($out_query[$i], '?') !== FALSE ) ? "'" .str_replace('?', $out_param[$j++]->getValue(), $out_query[$i]). "'" : $out_query[$i];
            } catch (\Exception $exception) {
                $mess = $exception->getMessage();
            } catch (\Error $exception) {
                $mess = $exception->getMessage();
            } catch (\RuntimeException $exception) {
                $mess = $exception->getMessage();
            } catch (\Throwable $exception) {
                $mess = $exception->getMessage();
            }
        }

        // output
        $sql = sprintf("%s", $output);

        return $sql;
    }
}