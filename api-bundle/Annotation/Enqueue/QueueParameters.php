<?php

namespace Webslon\Bundle\ApiBundle\Annotation\Enqueue;

/**
 * Class QueueParameters
 * @Annotation
 */
class QueueParameters extends Parameters
{
    /** @var bool */
    public $exclusive = false;
}