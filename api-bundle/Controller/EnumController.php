<?php

namespace Webslon\Bundle\ApiBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Webslon\Bundle\ApiBundle\Annotation\Resource;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Webslon\Bundle\ApiBundle\Service\CRUD\EnumService;
use Webslon\Library\Api\Response\BaseResponse;
use Webslon\Bundle\ApiBundle\Model\Response\EnumListResponse;

/**
 * Class EnumController
 * @Route("/api/enum/", methods={"GET"})
 * @Resource(
 *    tags={"Enum"}
 * )
 */
class EnumController extends AbstractController
{
    /**
     * @var EnumService
     */
    private $enumService;

    /**
     * EnumController constructor.
     *
     * @param EnumService $enumService
     */
    public function __construct(EnumService $enumService)
    {
        $this->enumService = $enumService;
    }

    /**
     * @Route("/", name="app.enum.list.all", methods={"GET"})
     * @SWG\Get(
     *     operationId="list",
     *     produces={"application/json"},
     *     summary="Получить список enum и их значений",
     *     description="Возвращает список enum",
     *     tags={"Enum"},
     *     @SWG\Parameter(
     *          name="filter",
     *          in="query",
     *          type="string",
     *          description="Json-объект с фильтром записей вида {&quot;column_name&quot;: &quot;value&quot;}"
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Возвращает коллекцию Enum",
     *          @Model(type=EnumListResponse::class)
     *     ),
     *     @SWG\Response(
     *          response="default",
     *          description="Error",
     *          @Model(type=BaseResponse::class)
     *     )
     * )
     * @param Request $request
     * @param EnumService $enumService
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAll(Request $request)
    {
        $filter = $request->get('filter');
        if (!$filter) {
            $items = $this->enumService->getAll();
        } else {
            $items = $this->enumService->getItemsByFilter(json_decode($filter, true));
        }

        return $this->json($items);
    }
}
