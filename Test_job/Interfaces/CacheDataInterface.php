<?php


namespace App\Interfaces;

use App\DTO\CacheItemDto;

/**
 * Interface CacheDataInterface
 */
interface CacheDataInterface
{
    /**
     * @param string $key
     */
    public function get($key);

    /**
     * @param CacheItemDto $itemDto
     */
    public function set(CacheItemDto $itemDto);

    /**
     * @param $key
     *
     * @return mixed
     */
    public function invalidateByKey(CacheItemDto $key);

    /**
     * @return mixed
     */
    public function invalidateAll();
}