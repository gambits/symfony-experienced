#!/bin/bash

while [ -n "$1" ]
do
    case "$1" in
        -repo) repo="$2"
        shift
        ;;
        -branch) branch="$2"
        shift
        ;;
        -msg) commitMessage="$2"
        shift
        ;;
        --) shift
        break ;;
        *) echo "$1 is not an option";;
esac
shift
done

if [ -z "${repo}" ]; then
    echo 'Не передан параметр -repo (путь к репозиторию)'
    exit 1
fi

if [ -z "${branch}" ]; then
    branch='dev'
fi

if [ -z "${commitMessage}" ]; then
    echo 'Не передан параметр -msg (сообщение комита)'
    exit 1
fi

# если есть папка с проектом - предварительно ее удалим
if [ -d "./workRepo" ]; then
    rm -Rf "./workRepo"
fi
mkdir "./workRepo"
cd "./workRepo"

# клонируем репозиторий микросервиса в текущую папку
git clone ${repo} . || exit "$?"
# tput setaf 2
echo "Repository cloned successful: ${repo}"
# tput sgr0

git reset --hard HEAD || exit "$?"
git fetch --all || exit "$?"
git checkout ${branch} || exit "$?"
git pull || exit "$?"

composer config --global discard-changes true || exit "$?"
composer update -n || exit "$?"

if [ -n "$(git status --porcelain)" ]; then
    git commit -am "${commitMessage}" || exit "$?"
    git push || exit "$?"
    echo "Microservice repository updated: ${repo}"
else
    echo "Microservice repository has no changes: ${repo}"
fi