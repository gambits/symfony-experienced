<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 10.04.18
 * Time: 7:02
 */

namespace Webslon\Bundle\ApiBundle\Model;

use Symfony\Component\PropertyInfo\Type;
use Webslon\Bundle\ApiBundle\Annotation\Resource;

final class Model extends \Nelmio\ApiDocBundle\Model\Model
{
    private $context;

    /**
     * @param Type          $type
     * @param string[]|null $groups
     * @param array $context
     */
    public function __construct(Type $type, array $groups = null, array $context = [])
    {
        parent::__construct($type, $groups);

        $this->context = $context;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @return mixed
     */
    public function getContextFor($field)
    {
        return !empty($this->context[$field]) ? $this->context[$field] : null;
    }

    /**
     * @param string|integer $field
     * @param mixed $value
     */
    public function addContext($field, $value)
    {
        $this->context[$field] = $value;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        $previousHash = parent::getHash();
        $hash = md5($previousHash.__CLASS__.json_encode($this->context));

        return $hash;
    }
}