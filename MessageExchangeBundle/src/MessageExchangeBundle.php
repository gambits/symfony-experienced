<?php

namespace Webslon\Bundle\MessageExchange;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class MessageExchangeBundle
 */
class MessageExchangeBundle extends Bundle
{
}