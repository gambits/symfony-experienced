<?php

namespace Webslon\Bundle\MessageExchange\Repository;

use Webslon\Bundle\MessageExchange\Entity\MessageExchange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MessageExchange|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageExchange|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageExchange[]    findAll()
 * @method MessageExchange[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageExchangeRepository extends ServiceEntityRepository
{
    /**
     * MessageExchangeRepository constructor.
     *
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MessageExchange::class);
    }
    
    /**
     * @param object $entity
     *
     * @return object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($entity)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();
        
        return $entity;
    }
    
    /**
     * @param int $id
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete($id)
    {
        if ($entity = $this->find($id)) {
            $this->getEntityManager()->remove($entity);
            $this->getEntityManager()->flush();
        }
        
        return true;
    }
}
