<?php

namespace Webslon\Bundle\ApiBundle\Controller\CRUD;

use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Webslon\Bundle\ApiBundle\Annotation\Operation;
use Webslon\Bundle\ApiBundle\Controller\ApiControllerInterface;
use Webslon\Bundle\ApiBundle\Service\CRUD\ReplaceItemService;
use Webslon\Bundle\ApiBundle\Service\SerializationContextFetcher;
use Webslon\Bundle\ApiBundle\Service\ValidationService;
use Webslon\Library\Api\Exception\ApiException;

/**
 * Trait ReplaceItemTrait
 */
trait ReplaceItemTrait
{
    /**
     * @Route("/{id}/replace", requirements={"id": "[a-z0-9\-\w+]{1,}"}, methods={"PUT"})
     * @Operation("put")
     * @SWG\Put(
     *     description="Замена сущности",
     *     summary="Замена сущности",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          description="Идентификатор заменяемой сущности",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="entity",
     *          in="body",
     *          description="Json-объект с данными заменяемой сущности, НЕ допустима передача сущности частично, только полностью",
     *          schema={}
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Успешный ответ сервиса, в результате приходят данные обновленного объекта",
     *          examples={
     *              "application/json":{
     *                  "status": true,
     *                  "response": {"id": 1},
     *                  "errors": null
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="Ошибка операции замены",
     *         examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": null,
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode": "ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     )
     * )
     *
     * @param string            $id
     * @param Request           $request
     * @param ReplaceItemService $service
     *
     * @return Response
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws ApiException
     * @throws \Exception
     */
    public function replaceItemAction(string $id, Request $request, ReplaceItemService $service, SerializationContextFetcher $serializationContextFetcher): Response
    {
        if ($this instanceof ApiControllerInterface) {
            $serializationContext = $this->getSerializationContext('patchItem');
        } else {
            $serializationContext = [];
        }

        $entityClass = $this instanceof DynamicEntityClassControllerInterface ? $this->getEntityClass() : self::ENTITY_CLASS;

        $isDefinedDtoClass = defined(get_class($this) . '::DTO_CLASS');
        if ($groups = $serializationContextFetcher->getSerializationGroups('patch', $isDefinedDtoClass ? self::DTO_CLASS : $entityClass)) {
            if (!isset($serializationContext['groups'])) {
                $serializationContext['groups'] = [];
            }

            $serializationContext['groups'] = array_merge($serializationContext['groups'], $groups);
        }

        $outputSerializationContext = $serializationContext;
        $outputSerializationContext['groups'] = $serializationContext['groups'] ?? [];
        $outputSerializationContext['groups'][] = 'detail';

        if ($isDefinedDtoClass) {
            $service->setDtoClass(self::DTO_CLASS);
        }

        $result = $service->replace($id, $request->getContent(), $entityClass, [ValidationService::GROUP_UPDATE_PUT_REPLACE, ValidationService::GROUP_DEFAULT], $outputSerializationContext, $serializationContext);

        return $result->send();
    }
}