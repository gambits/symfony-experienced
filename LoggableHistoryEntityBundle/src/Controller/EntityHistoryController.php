<?php

namespace Webslon\Bundle\WebslonEntityHistory\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;
use Webslon\Bundle\ApiBundle\Annotation\Resource;
use Webslon\Bundle\ApiBundle\Service\CRUD\GetListService;
use Webslon\Bundle\WebslonEntityHistory\Annotation\History;
use Webslon\Bundle\WebslonEntityHistory\Entity\HistoryEntity;
use Webslon\Bundle\WebslonEntityHistory\Service\EntityResolver;
use Webslon\Library\Api\Exception\ApiException;
use Webslon\Library\Api\Exception\Traits\CreateExceptionTranslationTrait;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Webslon\Bundle\WebslonEntityHistory\Model\EntityHistoryResponse;

/**
 * Class EntityHistoryController
 * @Resource(
 *    tags={"History"},
 *    description="Бизнес лог сущностей",
 * )
 */
class EntityHistoryController extends AbstractController
{
    use CreateExceptionTranslationTrait;
    const ENTITY_CLASS = HistoryEntity::class;

    /**
     * @var EntityResolver
     */
    private $resolver;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var GetListService
     */
    private $getListService;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * EntityHistoryController constructor.
     *
     * @param EntityResolver      $resolver
     * @param TranslatorInterface $translator
     * @param RequestStack        $request
     * @param GetListService      $getListService
     */
    public function __construct(
        EntityResolver $resolver,
        TranslatorInterface $translator,
        RequestStack $request,
        GetListService $getListService,
        EntityManagerInterface $em
    )
    {
        $this->resolver = $resolver;
        $this->translator = $translator;
        $this->request = $request->getCurrentRequest();
        $this->getListService = $getListService;
        $this->em = $em;
    }

    /**
     * @SWG\Get(
     *     summary="Получить весь бизнес-лог сущности",
     *     description="Получить весь бизнес-лог сущности",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="filter",
     *          in="query",
     *          type="string",
     *          description="Фильтрация по полям"
     *     ),
     *     @SWG\Parameter(
     *          name="aliasEntity",
     *          in="query",
     *          type="string",
     *          description="Алиас сущности",
     *          required=false
     *     ),
     *     @SWG\Parameter(
     *          name="id",
     *          in="query",
     *          type="string",
     *          description="ID сущности",
     *          required=false
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Успешный ответ сервиса, в результате приходят
     *     данные запрошенного объекта",
     *          examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": {},
     *                  "errors": null
     *              }
     *         },
     *         @Model(type=EntityHistoryResponse::class)
     *     ),
     *     @SWG\Response(
     *         response="default",
     *         description="Ошибка выполнения операции",
     *         examples={
     *              "application/json":{
     *                  "status": false,
     *                  "response": null,
     *                  "errors": {0: {"message": "Текст ошибки", "stringCode":
     *     "ERROR_STRING_CODE", "relatedField": null}}
     *              }
     *         },
     *         @Model(type=Webslon\Library\Api\Response\BaseResponse::class)
     *     )
     * )
     * @Route("/history/", options={}, methods={"GET"})
     * @return Response
     * @throws ApiException
     * @throws \Doctrine\ORM\Mapping\MappingException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \ReflectionException
     */
    public function list()
    {
        $filter = json_decode($this->request->get('filter'), true);
        $aliasEntity = $this->request->get('aliasEntity');
        $id = $this->request->get('id');
        if (isset($filter['id'])) {
            $id = $filter['id'];
        }
        if (isset($filter['aliasEntity'])) {
            $aliasEntity = $filter['aliasEntity'];
        }
        if ($aliasEntity) {
            /** @var History $historyAnnotation */
            if (!$historyAnnotation = $this->resolver->resolve($aliasEntity)) {
                $this->createException('error.alias_entity.not_found', [], Response::HTTP_INTERNAL_SERVER_ERROR, ['{alias}' => $aliasEntity]);
            }
            $filter = array_merge_recursive([
                '=objectClass' => $historyAnnotation->entity,
            ], json_decode($this->request->get('filter') ?? '[]', true));
        }
        if ($id) {
            $filter['=objectId'] = $id;
        }

        $this->request->query->add([
            'filter' => json_encode($filter),
        ]);

        return $this->getListService->getList($this->request, self::ENTITY_CLASS)->send();
    }

    /**
     * @param array $items
     * @param array $itemsRelation
     */
    public function addItems($items, &$itemsRelation)
    {
        foreach ($items as $item) {
            if (!empty($item)) {
                $itemsRelation[] = $item;
            }
        }
    }
}
