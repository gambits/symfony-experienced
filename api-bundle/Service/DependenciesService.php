<?php

namespace Webslon\Bundle\ApiBundle\Service;

use App\Model\V2\Region;
use App\Repository\OrderRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\ORM\PersistentCollection;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\TranslatorInterface;
use Webslon\Bundle\ApiBundle\Service\DTO\DTOFactory;
use Webslon\Library\Api\Exception\ApiException;
use Webslon\Library\Api\Response\BaseResponse as ApiResponse;

/**
 * Class DependenciesService
 */
class DependenciesService implements DependenciesInterface
{
    /** @var EntityManager */
    private $om;
    /** @var SerializerInterface */
    private $serializer;
    /** @var ApiResponse */
    private $response;
    /** @var ValidationService */
    private $validator;
    /** @var EventDispatcherInterface */
    private $dispatcher;
    /** @var RequestStack */
    private $request;
    /** @var LoggerInterface */
    private $logger;
    /** @var TranslatorInterface|Translator*/
    private $translator;
    /** @var DTOFactory */
    private $dtoFactory;
    /** @var array - arguments. for send to log */
    public $originalData;
    /**
     * DependenciesService constructor.
     *
     * @param EntityManagerInterface   $om
     * @param SerializerInterface      $serializer
     * @param ApiResponse              $response
     * @param ValidationService        $validator
     * @param EventDispatcherInterface $dispatcher
     * @param RequestStack             $request
     * @param LoggerInterface          $logger
     * @param Translator               $translator
     * @param DTOFactory|null          $dtoFactory
     */
    public function __construct(
        EntityManagerInterface $om,
        SerializerInterface $serializer,
        ApiResponse $response,
        ValidationService $validator,
        EventDispatcherInterface $dispatcher,
        RequestStack $request,
        LoggerInterface $logger,
        Translator $translator,
        ?DTOFactory $dtoFactory
    ) {
        $this->om = $om;
        $this->serializer = $serializer;
        $this->response = $response;
        $this->validator = $validator;
        $this->dispatcher = $dispatcher;
        $this->request = $request;
        $this->logger = $logger;
        $this->translator = $translator;
        $this->dtoFactory = $dtoFactory;
    }
    
    /**
     * @return ObjectManager
     */
    public function getOm(): ObjectManager
    {
        return $this->om;
    }
    
    /**
     * @return SerializerInterface
     */
    public function getSerializer(): SerializerInterface
    {
        return $this->serializer;
    }
    
    /**
     * @return ApiResponse
     */
    public function getResponse(): ApiResponse
    {
        return $this->response;
    }
    
    /**
     * @return ValidationService
     */
    public function getValidator(): ValidationService
    {
        return $this->validator;
    }
    
    /**
     * @return EventDispatcherInterface
     */
    public function getDispatcher(): EventDispatcherInterface
    {
        return $this->dispatcher;
    }
    
    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request->getCurrentRequest();
    }
    
    /**
     * @return Translator
     */
    public function getTranslator()
    {
        return $this->translator;
    }
    
    /**
     * @example
     *          for($entities as $index => $entity) {
     *              $this->saveEntity($entity, false); // persist (NOT flush - not insert/update to database)
     *              if($lastEntity) {
     *                  $this->saveEntity($entity, true); // // persist AND flush
     *              }
     *          }
     * @param object $entity
     * @param bool   $isFlush - for multiple query
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveEntity($entity, $isFlush = true)
    {
        try{
            // add entity to unit of work
            $this->om->persist($entity);
            $this->om->flush();
        }catch (UniqueConstraintViolationException $exception) {
            if (!$this->om->isOpen()) {
                $this->om = $this->om->create($this->om->getConnection(), $this->om->getConfiguration(), $this->om->getEventManager());
            }
            $this->getLogger()->debug($exception->getMessage(), [
                'originalData' => $this->originalData,
                'method' => __METHOD__,
                'entity' => get_class($entity),
                'typeException' => get_class($exception),
            ]);
            $ex = (new ApiException($exception->getMessage(), 'Error_500', null, Response::HTTP_INTERNAL_SERVER_ERROR))
                ->setErrorTrace($exception->getTraceAsString());
            $this->originalData = [];
            throw $ex;
        }
    }
    
    /**
     * @param $entity
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeEntity($entity)
    {
        $this->om->remove($entity);
        $this->om->flush();
    }
    
    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @return DTOFactory
     */
    public function getDtoFactory(): DTOFactory
    {
        return $this->dtoFactory;
    }
}