<?php

namespace Webslon\Bundle\MessageExchange\Command;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Webslon\Bundle\ApiBundle\GatewayConnector\GatewayClient;
use Webslon\Bundle\ApiBundle\Service\ValidationService;
use Webslon\Bundle\MessageExchange\Entity\MessageExchange;
use Webslon\Bundle\MessageExchange\Repository\MessageExchangeRepository;

/**
 * Class MessageQueueController
 */
class MessageQueueCommand extends Command
{
    use ContainerAwareTrait, LockableTrait;

    protected static $defaultName = 'message-exchange:run-queue';

    /**
     * @var GatewayClient
     */
    private $httpClient;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var MessageExchangeRepository
     */
    private $queueRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ValidationService
     */
    private $message;

    /**
     * @var ValidationService
     */
    private $validator;

    /**
     * MessageQueueCommand constructor.
     *
     * @param null|string               $name
     * @param MessageExchangeRepository $queueRepository
     * @param GatewayClient             $httpClient
     * @param EntityManagerInterface    $entityManager
     * @param LoggerInterface           $logger
     * @param ValidationService         $validator
     */
    public function __construct(?string $name = null, MessageExchangeRepository $queueRepository, GatewayClient $httpClient, EntityManagerInterface $entityManager, LoggerInterface $logger, ValidationService $validator)
    {
        $this->queueRepository = $queueRepository;
        $this->httpClient = $httpClient;
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->logger = $logger;

        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->setDescription('Handle queues query')
            ->addOption('remove-successful', 'rm', InputArgument::OPTIONAL, 'Удалить если успешно', true);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return;
        }

        $output->write('### START');

        try {
            $builder = $this->queueRepository->createQueryBuilder('me');

            $messages = $builder
                ->where($builder->expr()->in('me.status', [MessageExchange::STATUS_ERROR, MessageExchange::STATUS_PENDING]))
                ->setMaxResults(getenv('MESSAGE_EXCHANGE_MAX_ITEMS_ITERATE') ?? 50)
                ->getQuery()
                ->getResult();

            if (!$messages) {
                $output->writeln('No data for processing');
            }

            /** @var MessageExchange $message */
            foreach ($messages as $message) {
                $this->validator->validate($message, null, null, false, 'exception');

                $options = $message->getOptions();

                $request = $this->httpClient
                    ->setRoute($message->getRoute())
                    ->setBody($options['body'] ?? [])
                    ->setUseQueue(true);

                $request->setHeaders(array_merge($request->getHeaders() ?? [], $options['headers'] ?? []));
                $callback = $this->getCallback(strtolower($message->getMethod()));
                $result = $request->$callback();

                $messageLog = 'Запрос из MessageExchangeBundle:  ' . strtolower($message->getMethod()) . ' ' . $message->getRoute();
                if (isset($result['status'])) {
                    if ($result['status'] === true) {
                        $messageLog .= ' успешно выполнен';
                        $output->writeln('<info>' . $messageLog . '</info>');
                        $message->setStatus(MessageExchange::STATUS_FINISHED);
                        $this->entityManager->persist($message);
                        if ($input->getOption('remove-successful')) {
                            $this->entityManager->remove($message);
                        }
                    } else {
                        $message->setStatus(MessageExchange::STATUS_ERROR);
                        $messageLog .= ' завершился с ошибкой';
                        $output->writeln('<info>' . $messageLog . '</info>');
                        $this->logger->critical($messageLog, [
                            isset($result['errors']) ? json_encode($result['errors']) : json_encode($result),
                        ]);
                        $this->entityManager->persist($message);
                    }
                }
            }
            $this->entityManager->flush();
        } catch (\Throwable $ex) {
            $output->writeln($ex->getMessage());
        }

        $output->write('### FINISHED');
        $this->release();
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function getCallback($name)
    {
        switch ($name) {
            case 'post':
                $method = 'requestCrudPost';
                break;
            case 'get':
                $method = 'requestCrudGet';
                break;
            case 'patch':
                $method = 'requestCrudPatch';
                break;
            case 'delete':
                $method = 'requestCrudDelete';
                break;
        }

        return $method;
    }
}
