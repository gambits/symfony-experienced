<?php

namespace Webslon\Bundle\ApiBundle\Service\CRUD;

use Symfony\Component\HttpFoundation\Response;
use Webslon\Bundle\ApiBundle\EventDispatcher\EventRequest;
use Webslon\Bundle\ApiBundle\EventDispatcher\GetItemEvent;
use Webslon\Library\Api\Exception\ApiException;
use Webslon\Library\Api\Exception\Traits\CreateExceptionTranslationTrait;
use Webslon\Library\Api\Response\BaseResponse as ApiResponse;

/**
 * Class GetItemService
 */
class GetItemService extends AbstractService
{
    /**
     * @param string $id
     * @param string $object
     * @param string $attributes
     *
     * @return ApiResponse
     * @throws ApiException
     */
    public function getItem(string $id, string $object, string $attributes = null, array $serializationContext = []): ApiResponse
    {
        $eventName = $this->getEventName($object, 'getItem');
        $data = ['id' => $id, 'object' => $object, 'attributes' => $attributes, 'serializationContext' => $serializationContext];
        /** @var EventRequest $beforeEvent */
        $beforeEvent = $this->generateEvent($data, [EventRequest::BEFORE_PROCESS . ucfirst($eventName)]);
        $this->assertEvent($beforeEvent);
        if ($response = $beforeEvent->getResponse()) {
            $response = $this->getDependencies()->getResponse()
                ->setResponse($response);
            if ($beforeEvent->getHttpCode()) {
                $response->setHttpResponseCode($beforeEvent->getHttpCode());
            }
            return $response;
        }

        $entity = $this->getEntity($id, $object);
        if (
            ! $entity instanceof $object
            || (property_exists($entity, 'deleted') && $entity->isDeleted() === true)
        ) {
            $this->createException('error.message_not_found_entity.with_id', [], Response::HTTP_NOT_FOUND, [
                '{objectName}' => $this->translator->trans(substr(strrchr($object, "\\"), 1)),
                '{id}' => $id,
            ], null, 'classes');
        }

        if ($dtoClass = $this->getDtoClass()) {
            $dto = $this->dependencies->getDtoFactory()->getDTO($dtoClass);
            $dto->loadEntity($entity);
            $entity = $dto;
        }

        $getItemEvent = new GetItemEvent($entity, $object, $serializationContext);
        $this->getDependencies()->getDispatcher()->dispatch(GetItemEvent::NAME, $getItemEvent);

        if ($preparedResponse = $getItemEvent->getResponse()) {
            return $this->getDependencies()->getResponse()->setResponse($preparedResponse);
        }

        $entity = $getItemEvent->getItem();

        $response = $this->dependencies->getResponse()
            ->setSerializationContext($serializationContext)
            ->setResponse($entity);

        if ($attributes !== null && \is_array($attributes = json_decode($attributes, true))) {
            $response->setAttributes($attributes);
        }
        $this->generateEvent($entity, [EventRequest::AFTER_PROCESS . ucfirst($eventName)]);

        return $response;
    }
}
