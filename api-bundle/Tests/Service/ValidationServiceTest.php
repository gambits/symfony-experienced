<?php

namespace Webslon\Bundle\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Webslon\Bundle\ApiBundle\Service\ValidationService;
use Webslon\Bundle\ApiBundle\Tests\Service\Data\TestDto;

class ValidationServiceTest extends WebTestCase
{
    /**
     * @var ValidationService
     */
    private $validatorService;
    
    public function setUp()
    {
        parent::setUp();
        $this->validatorService = $this->createClient()->getContainer()->get('Webslon\Bundle\ApiBundle\Service\ValidationService');
    }
    
    public function testValidDto()
    {
        $dto = new TestDto();
        $dto->name = 0;
        $dto->counter = 'string';
        $violations = $this->validatorService->validate($dto, null, null, false);
        
        self::assertTrue(count($violations) == 2);
    }
}
