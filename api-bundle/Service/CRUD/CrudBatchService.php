<?php


namespace Webslon\Bundle\ApiBundle\Service\CRUD;

use Ramsey\Uuid\Uuid;
use Webslon\Bundle\ApiBundle\AMQP\Packet;
use Webslon\Bundle\ApiBundle\AMQP\Producer;
use Webslon\Bundle\ApiBundle\AMQP\Router\Route;
use Webslon\Bundle\ApiBundle\AMQP\RPC\Exchange;
use Webslon\Library\Api\Response\BaseResponse;
use Doctrine\Common\Annotations\AnnotationReader;
use Webslon\Bundle\ApiBundle\Annotation\Enqueue\Consume;
use Webslon\Bundle\ApiBundle\Annotation\Enqueue\CrudBatchConsume;
use Webslon\Bundle\ApiBundle\Service\CRUD\Interfaces\CrudBatchServiceInterface;
use Webslon\Bundle\ApiBundle\Exception\AmqpEmptyBatchAnnotationException;

/**
 * Class CrudBatchService
 */
class CrudBatchService implements CrudBatchServiceInterface
{
    /**
     * @var AnnotationReader
     */
    public $reader;
    /**
     * @var Producer
     */
    public $producer;

    /**
     * CrudBatchService constructor.
     *
     * @param AnnotationReader     $reader
     * @param Producer             $producer
     */
    public function __construct(AnnotationReader $reader, Producer $producer)
    {
        $this->reader = $reader;
        $this->producer = $producer;
    }

    /**
     * @param string      $data
     * @param string      $entityName
     * @param string|null $dtoName
     * @param array|null  $params
     *
     * @return mixed|void|BaseResponse
     * @throws AmqpEmptyBatchAnnotationException
     * @throws \Exception
     */
    public function handleCreate($data, $entityName, $dtoName = null, ?array $params = [])
    {
        if ($exchange = $this->getExchange($entityName, Route::TOPIC_CREATE_PREFIX)) {
            $correlationId = Uuid::uuid4();
            $replyTo = $this->producer->getRpcManager()->declareRpcCallbackQueue($this->producer->getChannel(), $correlationId);

            return $this->producer->rpcCallAsync($exchange, $this->createPacket(Route::TOPIC_CREATE_PREFIX, $data,  $entityName, $dtoName), $correlationId, $replyTo);
        }
    }

    /**
     * @param string      $data
     * @param string      $entityName
     * @param string|null $dtoName
     * @param array|null  $params
     *
     * @return mixed|void|BaseResponse
     * @throws AmqpEmptyBatchAnnotationException
     * @throws \Exception
     */
    public function handleUpdate($data, $entityName, $dtoName = null, ?array $params = [])
    {
        if ($exchange = $this->getExchange($entityName, Route::TOPIC_UPDATE_PREFIX)) {
            $correlationId = Uuid::uuid4();
            $replyTo = $this->producer->getRpcManager()->declareRpcCallbackQueue($this->producer->getChannel(), $correlationId);

            return $this->producer->rpcCallAsync($exchange, $this->createPacket(Route::TOPIC_UPDATE_PREFIX, $data,  $entityName, $dtoName), $correlationId, $replyTo);
        }
    }

    /**
     * @param int|string  $id
     * @param string      $data
     * @param string      $entityName
     * @param string|null $dtoName
     * @param array|null  $params
     *
     * @return mixed|void|BaseResponse
     * @throws AmqpEmptyBatchAnnotationException
     * @throws \Exception
     */
    public function handleDelete($data, $entityName, $dtoName = null, ?array $params = [])
    {
        if ($exchange = $this->getExchange($entityName, Route::TOPIC_DELETE_PREFIX)) {
            $correlationId = Uuid::uuid4();
            $replyTo = $this->producer->getRpcManager()->declareRpcCallbackQueue($this->producer->getChannel(), $correlationId);

            return $this->producer->rpcCallAsync($exchange, $this->createPacket(Route::TOPIC_DELETE_PREFIX, $data,  $entityName, $dtoName), $correlationId, $replyTo);
        }
    }

    /**
     * @param string      $data
     * @param string      $entityName
     * @param string|null $dtoName
     * @param array|null  $params
     *
     * @return mixed|void|BaseResponse
     * @throws AmqpEmptyBatchAnnotationException
     * @throws \Exception
     */
    public function handleReplace($data, $entityName, $dtoName, ?array $params = [])
    {
        if ($exchange = $this->getExchange($entityName, Route::TOPIC_REPLACE_PREFIX)) {
            $correlationId = Uuid::uuid4();
            $replyTo = $this->producer->getRpcManager()->declareRpcCallbackQueue($this->producer->getChannel(), $correlationId);

            return $this->producer->rpcCallAsync($exchange, $this->createPacket(Route::TOPIC_REPLACE_PREFIX, $data,  $entityName, $dtoName), $correlationId, $replyTo);
        }
    }

    /**
     * @param string $entityNamespace
     * @param string $action
     *
     * @throws AmqpEmptyBatchAnnotationException
     * @throws \ReflectionException
     * @return Consume
     */
    private function resolveAmqpData($entityNamespace, $action):Consume
    {
        /** @var CrudBatchConsume $annotationCrudBatch */
        $annotationCrudBatch = $this->reader->getClassAnnotation(new \ReflectionClass($entityNamespace), CrudBatchConsume::class);
        if (!isset($annotationCrudBatch->topicsMap[$action])) {
            throw new AmqpEmptyBatchAnnotationException('error.amqp.entity.empty.batch_annotation', 'Error_500', null, 500, ['{entityName}' => $entityNamespace]);
        }

        return $annotationCrudBatch->topicsMap[$action];
    }

    /**
     * @param $entityName
     *
     * @return string|Exchange
     * @throws AmqpEmptyBatchAnnotationException
     * @throws \ReflectionException
     */
    private function getExchange($entityName, $action)
    {
        $amqpData = $this->resolveAmqpData($entityName, $action);

        if ($amqpData->exchangeName && $amqpData->exchangeBindKey) {
            list($exchangeName) = $this->producer->getPlaceholderResolver()->handlePlaceholdersParameters($amqpData->exchangeName);
            list($exchangeBindKey) = $this->producer->getPlaceholderResolver()->handlePlaceholdersParameters($amqpData->exchangeBindKey);
            $exchange = new Exchange($exchangeName, $exchangeBindKey);
        } else {
            list($exchange) = $this->producer->getPlaceholderResolver()->handlePlaceholdersParameters($amqpData->queue);
        }

        return $exchange;
    }

    /**
     * @param string     $action
     * @param string     $data
     * @param string     $entityName
     * @param string     $dtoName
     *
     * @return Packet
     */
    private function createPacket($action, $data, $entityName, $dtoName)
    {
        return Packet::createFromData([
            'action' => $action,
            'entity' => $entityName,
            'dtoName' => $dtoName,
            'package' => json_decode($data, true),
        ]);
    }
}