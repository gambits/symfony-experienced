<?php

namespace Webslon\Bundle\ApiBundle\EventSubscriber;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Sentry\SentryBundle\Event\SentryUserContextEvent;
use Sentry\SentryBundle\SentrySymfonyEvents;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Class SentrySubscriber
 */
class SentrySubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            SentrySymfonyEvents::PRE_CAPTURE => 'onPreCapture',
            SentrySymfonyEvents::SET_USER_CONTEXT => 'onSetUserContext',
        ];
    }

    public function onSetUserContext(SentryUserContextEvent $event)
    {
    }

    public function onPreCapture(Event $event)
    {
        if ($event instanceof GetResponseForExceptionEvent) {
            $exception = $event->getException();
        } elseif ($event instanceof ConsoleErrorEvent) {
            $exception = $event->getError();
        } else {
            return;
        }

        switch (true) {
            case $exception instanceof ConnectException:
                $host = $exception->getRequest()->getUri()->getHost();
                if (strpos($exception->getMessage(), 'Operation timed out after') !== false) {
                    $message = sprintf('Таймаут при запросе к %s', $host);
                } elseif(strpos($exception->getMessage(), 'Connection refused') !== false) {
                    $message = sprintf('В соединении отказано при подключении к %s', $host);
                } else {
                    $message = $exception->getMessage();
                }
                break;

            case $exception instanceof RequestException:
                $message = sprintf(
                    '%s %s %s',
                    $exception->getRequest()->getUri()->getHost(),
                    $exception->getResponse()->getStatusCode(),
                    $exception->getResponse()->getReasonPhrase()
                );
                break;

            default:
                return;
                break;
        }

        $reflectionProperty = new \ReflectionProperty(get_class($exception), 'message');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($exception, $message);
    }
}