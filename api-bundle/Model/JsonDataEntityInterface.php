<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 28.02.19
 * Time: 9:31
 */

namespace Webslon\Bundle\ApiBundle\Model;

/**
 * Interface JsonDataEntityInterface
 */
interface JsonDataEntityInterface
{
    public function getJsonData();
    public function setJsonData(array $data);
}
