<?php
/**
 * Created by PhpStorm.
 * User: anboo
 * Date: 17.10.18
 * Time: 9:42
 */

namespace Webslon\Bundle\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Webslon\Bundle\AuthBundle\Endpoint\Client\AccountClient;
use Webslon\Bundle\AuthBundle\Security\Token\OAuthPreAuthenticatedToken;

/**
 * Class OAuthRedirectController
 */
class OAuthRedirectController extends Controller
{
    public function swaggerOAuthAction()
    {
        return $this->render('@WebslonApi/SwaggerUi/oauth_redirect.html.twig');
    }
}