<?php

namespace Webslon\Bundle\ApiBundle\Monolog;

use PhpAmqpLib\Message\AMQPMessage;
use PHPUnit\Runner\Exception;
use Webslon\Bundle\ApiBundle\AMQP\Packet;
use Webslon\Bundle\ApiBundle\Command\ConsumerCommand;

class RabbitMqContext
{
    /**
     * @param Packet $packet
     * @param Exception $e
     * @return array
     */
    public static function getLoggingContext(Packet $packet, $e = null)
    {
        return self::getLoggingContextAmqpMessage($packet->getAMQPMessage(), $e);
    }

    /**
     * @param AMQPMessage $AMQPMessage
     * @param Exception $e
     * @return array
     */
    public static function getLoggingContextAmqpMessage(AMQPMessage $AMQPMessage, $e = null) : array
    {
        $context = [
            'tags' => [
                'exchange' => $AMQPMessage->delivery_info['exchange'] ?? null,
                'routing_key' => $AMQPMessage->delivery_info['routing_key'] ?? null,
                'command' => ConsumerCommand::getDefaultName(),
            ],
            'body' => $AMQPMessage->getBody() ?? null,
            'exception' => $e,
        ];

        return array_filter($context, function($item) { return $item !== null || $item === 0; });
    }
}