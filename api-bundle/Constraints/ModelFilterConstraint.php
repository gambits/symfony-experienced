<?php

namespace App\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Webslon\Bundle\ApiBundle\Service\CRUD\Filters\FilterModelInterface;

/**
 * Class ModelFilterConstraint
 */
class ModelFilterConstraint extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$value instanceof FilterModelInterface) {
            return;
        }
        
    }
}
