<?php

namespace Webslon\Bundle\ApiBundle\Service\CRUD\Interfaces;

use \Webslon\Library\Api\Response\BaseResponse;

/**
 * Interface CrudBatchServiceInterface
 */
interface CrudBatchServiceInterface
{
    /**
     * @param string $data
     * @param string $entityName
     * @param string $dtoName
     * @param array  $params
     *
     * @return mixed|BaseResponse
     */
    public function handleCreate($data, $entityName, $dtoName, ?array $params = []);
    /**
     * @param string     $data
     * @param string     $entityName
     * @param string     $dtoName
     * @param array      $params
     *
     * @return mixed|BaseResponse
     */
    public function handleUpdate($data, $entityName, $dtoName, ?array $params = []);
    /**
     * @param string     $data
     * @param string     $entityName
     * @param string     $dtoName
     * @param array      $params
     *
     * @return mixed|BaseResponse
     */
    public function handleDelete($data, $entityName, $dtoName, ?array $params = []);
    /**
     * @param string     $data
     * @param string     $entityName
     * @param string     $dtoName
     * @param array      $params
     *
     * @return mixed|BaseResponse
     */
    public function handleReplace($data, $entityName, $dtoName, ?array $params = []);
}