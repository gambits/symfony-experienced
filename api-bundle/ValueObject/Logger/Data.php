<?php

namespace Webslon\Bundle\ApiBundle\ValueObject\Logger;

class Data
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var string
     */
    private $response;

    /**
     * @var string
     */
    private $requestDate;

    /**
     * @var string
     */
    private $responseDate;

    public function __construct(
        Request $request,
        string $response,
        \DateTimeImmutable $requestDate,
        \DateTimeImmutable $responseDate
    ) {
        $this->request = $request;
        $this->response = $response;
        $this->requestDate = $requestDate->format(\DateTime::ATOM);
        $this->responseDate = $responseDate->format(\DateTime::ATOM);
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return string
     */
    public function getResponse(): string
    {
        return $this->response;
    }

    /**
     * @return string
     */
    public function getRequestDate(): string
    {
        return $this->requestDate;
    }

    /**
     * @return string
     */
    public function getResponseDate(): string
    {
        return $this->responseDate;
    }
}
